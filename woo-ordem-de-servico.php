<?php
defined( 'ABSPATH' ) || exit;
/**
 * Plugin Name: Woo Ordem de Serviço
 * Plugin URI: https://3xweb.site
 * Description: Crie e gerencie ordens de serviço anexáveis aos pedidos do woocommerce
 * Author URI: https://3xweb.site/
 * Version: 1.0.0
 * Requires at least: 4.4
 * Tested up to: 5.7
 * WC requires at least: 3.0
 * WC tested up to: 5.4
 * Text Domain: woo-orderm-de-servico
 * Domain Path: /languages
 */

/**
 * Required minimums and constants
 */
define( 'WOO_ORDEM_DE_SERVICO_VERSION', '5.3.0' ); // WRCS: DEFINED_VERSION.
define( 'WOO_ORDEM_DE_SERVICO_MIN_PHP_VER', '5.6.0' );
define( 'WOO_ORDEM_DE_SERVICO_MIN_WC_VER', '3.0' );
define( 'WOO_ORDEM_DE_SERVICO_MAIN_FILE', __FILE__ );
define( 'WOO_ORDEM_DE_SERVICO_PLUGIN_URL', untrailingslashit( plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) ) );
define( 'WOO_ORDEM_DE_SERVICO_PLUGIN_PATH', untrailingslashit( plugin_dir_path( __FILE__ ) ) );

/**
 * Desativa o plugin caso não seja compatível.
 *
 * @since 1.0.0
 */
function deactivate_this_plugin(){
	// do_action( 'deactivated_plugin', __FILE__, false );
	deactivate_plugins( plugin_basename( __FILE__ ) );
	wp_die( 'Os requisitos míninos de funcionamento não foram atendidos, instale as depêndencias antes de prosseguir' );
	// die('Please Upgrade your WordPress to use this plugin.');
}
/**
 * WooCommerce fallback notice.
 *
 * @since 4.1.2
 */
function woo_ordem_de_servico_missing_wc_notice() {
	/* translators: 1. URL link. */
	echo '<div class="error"><p><strong>' . sprintf( esc_html__( 'Woo Ordem de serviço precisa que o WooCommerce esteja instalado e ativo. Você pode baixar o %s aqui.', 'woo-orderm-de-servico' ), '<a href="https://woocommerce.com/" target="_blank">WooCommerce</a>' ) . '</strong></p></div>';
	deactivate_this_plugin();
}

function woo_ordem_de_servico_missing_br_notice() {
	/* translators: 1. URL link. */
	echo '<div class="error"><p><strong>' . sprintf( esc_html__( 'Woo Ordem de serviço precisa que o Brazilian Market on WooCommerce esteja instalado e ativo. Você pode baixar o %s aqui.', 'woo-orderm-de-servico' ), '<a href="https://br.wordpress.org/plugins/woocommerce-extra-checkout-fields-for-brazil/" target="_blank">Brazilian Market on WooCommerce</a>' ) . '</strong></p></div>';
	deactivate_this_plugin();
}
function woo_ordem_de_servico_missing_br_configuration() {
	/* translators: 1. URL link. */
	echo '<div class="error"><p><strong>' . sprintf(  'Para que as OS funcionem corretamente você deve configurar o Brazilian Market for Woocommerce para permitir:
		<ol>
		<li>Cadastro de PF e PJ</li>
		<li>Exibir IE para PJ</li>
		<li>Exibir Data de Nascimento e Gênero</li>
		<li>Exibir Celular</li>
		</ol>
		Visite a página de <a href="/wp-admin/admin.php?page=woocommerce-extra-checkout-fields-for-brazil" target="_blank">Configurações</a>' ) . '</strong></p></div>';
	deactivate_this_plugin();
}

/**
 * WooCommerce not supported fallback notice.
 *
 * @since 4.4.0
 */
function woo_ordem_de_servico_not_supported() {
	/* translators: $1. Minimum WooCommerce version. $2. Current WooCommerce version. */
	echo '<div class="error"><p><strong>' . sprintf( esc_html__( 'Woo Ordem de serviço precisa do WooCommerce %1$s or superior instalado e ativo. WooCommerce %2$s não é suportado.', 'woo-orderm-de-servico' ), WOO_ORDEM_DE_SERVICO_MIN_WC_VER, WC_VERSION ) . '</strong></p></div>';
	deactivate_this_plugin();
}

function woo_ordem_de_servico() {

	static $plugin;

	if ( ! isset( $plugin ) ) {

		class WC_OS {

			/**
			 * The *Singleton* instance of this class
			 *
			 * @var Singleton
			 */
			private static $instance;

			/**
			 * Returns the *Singleton* instance of this class.
			 *
			 * @return Singleton The *Singleton* instance.
			 */
			public static function get_instance() {
				if ( null === self::$instance ) {
					self::$instance = new self();
				}
				return self::$instance;
			}

			/**
			 * Private clone method to prevent cloning of the instance of the
			 * *Singleton* instance.
			 *
			 * @return void
			 */
			public function __clone() {}

			/**
			 * Private unserialize method to prevent unserializing of the *Singleton*
			 * instance.
			 *
			 * @return void
			 */
			public function __wakeup() {}

			/**
			 * Protected constructor to prevent creating a new instance of the
			 * *Singleton* via the `new` operator from outside of this class.
			 */
			public function __construct() {
				add_action( 'admin_init', [ $this, 'install' ] );

				if ( class_exists( 'WooCommerce' ) ) {
					if ( is_admin() ) {
						$this->admin_includes();
					}

					$this->includes();
					// add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'plugin_action_links' ) );
				} else {
					echo 'error loading this';
				}
			}
			/**
			 * Handles upgrade routines.
			 *
			 * @since 3.1.0
			 * @version 3.1.0
			 */
			public function install() {
				if ( ! is_plugin_active( plugin_basename( __FILE__ ) ) ) {
					return;
				}

			}
			private function includes() {
				include_once dirname( __FILE__ ) . '/includes/frontend/woo-os-frontend-data.php';
				include_once dirname( __FILE__ ) . '/includes/frontend/woo-os-frontend-endpoint.php';
				include_once dirname( __FILE__ ) . '/includes/frontend/woo-os-single-endpoint.php';
				include_once dirname( __FILE__ ) . '/includes/woo-os-order-status-updater.php';
				include_once dirname( __FILE__ ) . '/includes/woo-os-order-updater.php';
				include_once dirname( __FILE__ ) . '/includes/woo-os-notas.php';

				include_once dirname( __FILE__ ) . '/includes/addons/woo-os-wt-advanced-order-number.php';
				// include_once dirname( __FILE__ ) . '/includes/addons/woo-os-pay-order-anon.php';
				include_once dirname( __FILE__ ) . '/includes/frontend/woo-os-checkout-messages.php';
			    include_once dirname( __FILE__ ) . '/includes/frontend/woo-os-template-support.php';
			    // SHORTCODES
			    include_once dirname( __FILE__ ) . '/includes/shortcodes/woo-os-pesquisa-shortcode.php';
				// include_once dirname( __FILE__ ) . '/templates/views/html-print-os.';
				// include_once dirname( __FILE__ ) . '/includes/class-extra-checkout-fields-for-brazil-formatting.php';
				// include_once dirname( __FILE__ ) . '/includes/class-extra-checkout-fields-for-brazil-front-end.php';
				// include_once dirname( __FILE__ ) . '/includes/class-extra-checkout-fields-for-brazil-integrations.php';
				// include_once dirname( __FILE__ ) . '/includes/class-extra-checkout-fields-for-brazil-api.php';
			}

			/**
			 * Admin includes.
			 */
			private function admin_includes() {
				include_once dirname( __FILE__ ) . '/includes/woo-os-custom-post.php';
				include_once dirname( __FILE__ ) . '/includes/woo-os-ajax.php';
				include_once dirname( __FILE__ ) . '/includes/woo-os-custom-order-metabox.php';
				// include_once dirname( __FILE__ ) . '/includes/woo-os-notas.php';
				// include_once dirname( __FILE__ ) . '/includes/admin/class-extra-checkout-fields-for-brazil-settings.php';
				// include_once dirname( __FILE__ ) . '/includes/admin/class-extra-checkout-fields-for-brazil-order.php';
				// include_once dirname( __FILE__ ) . '/includes/admin/class-extra-checkout-fields-for-brazil-customer.php';
			}

		}

		$plugin = WC_OS::get_instance();

	}

	return $plugin;
}
// carregando scripts adicionais
add_action( 'admin_enqueue_scripts', '_3xweb_woo_os_scriptLoader' );

function _3xweb_woo_os_scriptLoader(){
    // Register each script
    wp_register_script(
        'woo_os_javascript', 
        plugins_url('/resources/js/woo-os-javascript.js', __FILE__ ), 
        array( 'jquery' ),
        false,
        true
    );
    wp_register_script(
        'woo_os_jquery_mask', 
        plugins_url('/resources/js/jquery.mask/jquery.mask.min.js', __FILE__ ), 
        array( 'jquery' ),
        false,
        true
    );
    wp_register_style('woo_os_css', plugins_url('/resources/css/style.css',__FILE__ ));
    wp_enqueue_script('woo_os_javascript');
    wp_enqueue_script('woo_os_jquery_mask');
    wp_enqueue_style('woo_os_css');
    wp_enqueue_style('dashicons');
}

/**
 * Carregando scripts publicos
 */
function _3xweb_woo_os_publicScriptLoader($hook) {
 
    // create my own version codes
    $my_js_ver  = date("ymd-Gis", filemtime( plugin_dir_path( __FILE__ ) . '/resources/js/' ));
     
    // 
    wp_enqueue_script( 'woo_os_public_js', plugins_url( '/resources/js/public-scripts.js', __FILE__ ), array(), $my_js_ver );

    wp_localize_script( 'woo_os_public_js', 'my_ajax_object',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
 
}
add_action('wp_enqueue_scripts', '_3xweb_woo_os_publicScriptLoader');

// Permite pagamentos sem login do usuário
function woo_os_allow_payment_without_login( $allcaps, $caps, $args ) {
    // Check we are looking at the WooCommerce Pay For Order Page
    if ( !isset( $caps[0] ) || $caps[0] != 'pay_for_order' )
        return $allcaps;
    // Check that a Key is provided
    if ( !isset( $_GET['key'] ) )
        return $allcaps;

    // Find the Related Order
    $order = wc_get_order( $args[2] );
    if( !$order )
        return $allcaps; # Invalid Order

    // Get the Order Key from the WooCommerce Order
    $order_key = $order->get_order_key();
    // Get the Order Key from the URL Query String
    $order_key_check = $_GET['key'];

    // Set the Permission to TRUE if the Order Keys Match
    $allcaps['pay_for_order'] = ( $order_key == $order_key_check );

    return $allcaps;
}
add_filter( 'user_has_cap', 'woo_os_allow_payment_without_login', 10, 3 );

// iniciando o plugin e verificando as dependências
add_action( 'plugins_loaded', 'woo_ordem_de_servico_init' );

function woo_ordem_de_servico_init() {
	require_once( ABSPATH . 'wp-admin/includes/plugin.php' );


	load_plugin_textdomain( 'woo-orderm-de-servico', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );

	if ( ! class_exists( 'WooCommerce' ) ) {
		add_action( 'admin_notices', 'woo_ordem_de_servico_missing_wc_notice' );
		return;
	}

	if ( ! class_exists( 'Extra_Checkout_Fields_For_Brazil' ) ) {
		add_action( 'admin_notices', 'woo_ordem_de_servico_missing_br_notice' );
		return;
	}

	if ( version_compare( WC_VERSION, WOO_ORDEM_DE_SERVICO_MIN_WC_VER, '<' ) ) {
		add_action( 'admin_notices', 'woo_ordem_de_servico_not_supported' );
		return;
	}
	/*	campos obrigatórios do brmfwc
		(
		    [person_type] => 1
		    [ie] => 1
		    [birthdate_sex] => 1
		    [cell_phone] => 1
		   
		)
	*/
	// verifica se os campos do Brazilian Market estão configurados de forma favorável
	$wcbcf_st  = get_option( 'wcbcf_settings' );
   	
   		
   		if(!isset($wcbcf_st['person_type']) || $wcbcf_st['person_type'] !== '1' ){
   			add_action( 'admin_notices', 'woo_ordem_de_servico_missing_br_configuration' );
   			return;
   		}
   		if(!isset($wcbcf_st['ie']) || $wcbcf_st['ie'] !== '1' ){
   			add_action( 'admin_notices', 'woo_ordem_de_servico_missing_br_configuration' );
   			return;
   		}
   		if(!isset($wcbcf_st['birthdate_sex']) || $wcbcf_st['birthdate_sex'] !== '1' ){
   			add_action( 'admin_notices', 'woo_ordem_de_servico_missing_br_configuration' );
   			return;
   		}
   		if(!isset($wcbcf_st['cell_phone']) || $wcbcf_st['cell_phone'] !== '1' ){
   			add_action( 'admin_notices', 'woo_ordem_de_servico_missing_br_configuration' );
   			return;
   		}  		

	woo_ordem_de_servico();
}
?>