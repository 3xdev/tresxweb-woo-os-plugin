<?php
defined( 'ABSPATH' ) || exit;
/*
* Funcão para criar e registrar o novo tipo de post
*/
 
function woo_ordem_de_servico_custom_post_type() {
 
// TODOS OS RÓTULOS DO PAINEL ADMIN
    $labels = array(
        'name'                => __( 'Ordens de Serviço', 'Post Type General Name', 'twentytwenty' ),
        'singular_name'       => __( 'Ordem de Serviço', 'Post Type Singular Name', 'twentytwenty' ),
        'menu_name'           => __( 'OS', 'twentytwenty' ),
        'parent_item_colon'   => __( 'OS Pai', 'twentytwenty' ),
        'all_items'           => __( 'Todas as OS', 'twentytwenty' ),
        'view_item'           => __( 'Ver OS', 'twentytwenty' ),
        'add_new_item'        => __( 'Adicionar OS', 'twentytwenty' ),
        'add_new'             => __( 'Adicionar Nova', 'twentytwenty' ),
        'edit_item'           => __( 'Editar OS', 'twentytwenty' ),
        'update_item'         => __( 'Atualizar OS', 'twentytwenty' ),
        'search_items'        => __( 'Pesquisar OS', 'twentytwenty' ),
        'not_found'           => __( 'Não encontrada', 'twentytwenty' ),
        'not_found_in_trash'  => __( 'Não encontrada no lixo', 'twentytwenty' ),
    );
     
// OUTRAS OPCOES DO CPT
     
    $args = array(
        'label'               => __( 'os', 'twentytwenty' ),
        'description'         => __( 'Ordens de serviço de clientes', 'twentytwenty' ),
        'labels'              => $labels,
        // SUPORTE
        'supports'            => array( 'author' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        // 'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => false,
        'capability_type'     => 'post',
        'show_in_rest' => true,
        // metaboxes
        'register_meta_box_cb' => 'woo_os_add_metaboxes',
    );
     
    // REGISTRA O CPT
    register_post_type( 'ordens_de_servico', $args );
 
}
 
/* Hook no 'init' action para executar a função
*/
 
add_action( 'init', 'woo_ordem_de_servico_custom_post_type', 0 );

/**
 * Definição dos metaboxes
 */
function woo_os_add_metaboxes() {

    
    add_meta_box(
        'woo_os_client',
        'Dados do cliente',
        'woo_os_cliente_data',
        'ordens_de_servico',
        'normal',
        'default'
    );

    add_meta_box(
        'woo_os_data',
        'Dados da OS',
        'woo_os_os_data',
        'ordens_de_servico',
        'normal',
        'default'
    );

    add_meta_box(
        'woo_os_orcamento',
        'Dados do Orçamento',
        'woo_os_orcamento',
        'ordens_de_servico',
        'normal',
        'default'
    );

    add_meta_box(
        'woo_os_order_data',
        'Dados do pedido',
        'woo_os_order_data',
        'ordens_de_servico',
        'side',
        'default'
    );

    add_meta_box( 
        'woo_os_notas', 
        'Histórico da OS', 
        'WOO_OS_Box_Order_Notes::output', 
        'ordens_de_servico', 
        'side', 
        'default' 
    );

    remove_meta_box('commentstatusdiv', 'ordens_de_servico', 'normal');
    remove_meta_box('commentsdiv', 'ordens_de_servico', 'normal');
    remove_meta_box('tagsdiv-woo_os_marcas', 'ordens_de_servico', 'side');
    // remove_meta_box('commentsdiv', 'ordens_de_servico', 'normal');

}
// remove as opções de editar, apagar e visualar na tela de Todas as OS no admin
function woo_os_remove_quick_edit_admin_tabs( $actions, $post ){
    global $post;

    if ($post->post_type=='ordens_de_servico') {
         unset($actions['edit']);
         unset($actions['trash']);
         unset($actions['view']);
         unset($actions['inline hide-if-no-js']);
    }
             
         return $actions;
    }
add_filter('post_row_actions','woo_os_remove_quick_edit_admin_tabs',10,2);

/**
 * Removes the "Trash" link on the individual post's "actions" row on the posts
 * edit page.
 */

// remove a opção de apagar ou clonar o post
add_filter( 'post_row_actions', 'woo_os_remove_row_actions_post', 10, 2 );
function woo_os_remove_row_actions_post( $actions, $post ) {
    if( $post->post_type === 'ordens_de_servico' ) {
        unset( $actions['clone'] );
        unset( $actions['trash'] );
    }
    return $actions;
}
// avisa que o post é protegido caso alguem tente apagar
add_action('wp_trash_post', 'woo_os_restrict_post_deletion');
function woo_os_restrict_post_deletion($post_id) {
    if( get_post_type($post_id) === 'ordens_de_servico' ) {
      wp_die('The post you were trying to delete is protected.');
    }
}
/**
 * Removes the "Delete" link on the individual term's "actions" row on the terms
 * edit page.
 */
// remove a opção de apagar o post
add_action( 'admin_head', function () {
    $current_screen = get_current_screen();

    // Hides the "Move to Trash" link on the post edit page.
    if ( 'post' === $current_screen->base &&
    'ordens_de_servico' === $current_screen->post_type ) :
    ?>
        <style>#delete-action, .actions .bulkactions { display: none; }</style>
    <?php
    endif;
    if ( 'edit' === $current_screen->base &&
    'ordens_de_servico' === $current_screen->post_type ) :
    ?>
        <style>.bulkactions { display: none; }</style>
    <?php
    endif;
} );
// não pertmite que a data seja editada
function woo_os_deny_post_date_change( $data, $postarr ) {
  unset( $data['post_date'] );
  unset( $data['post_date_gmt'] );
  return $data;
}
// add_filter( 'wp_insert_post_data', 'woo_os_deny_post_date_change', 0, 2 );

// esconde o controle de edição do post
function woo_os_hide_publication_date_elements() { 
     $current_screen = get_current_screen();
    if ( 'post' === $current_screen->base &&
    'ordens_de_servico' === $current_screen->post_type ) :
    ?>
      <style type="text/css">
        a.edit-timestamp, #timestampdiv {display:none;}
      </style>
      <?php
  endif;
}
add_action( 'admin_enqueue_scripts', 'woo_os_hide_publication_date_elements' );
//HOOK no itin e cria a taxonomia de marcas para o tipo de post 'ordens_de_servico'
add_action( 'init', 'woo_os_marca_taxonomy', 0 );
 
function woo_os_marca_taxonomy() {
 
// Labels part for the GUI
 
  $labels = array(
    'name' => _x( 'Marcas', 'taxonomy general name' ),
    'singular_name' => _x( 'Marca', 'taxonomy singular name' ),
    'search_items' =>  __( 'Pesquisar Marcas' ),
    'popular_items' => __( 'Marcas Populares' ),
    'all_items' => __( 'Todas as Marcas' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Editar Marca' ), 
    'update_item' => __( 'Atualizar Marca' ),
    'add_new_item' => __( 'Adicionar Nova Marca' ),
    'new_item_name' => __( 'Nome da Nova Marca' ),
    'separate_items_with_commas' => __( 'Separe as marcas por vírgula' ),
    'add_or_remove_items' => __( 'Adicione ou remova marcas' ),
    'choose_from_most_used' => __( 'Escolher entre as marcas mais usadas' ),
    'menu_name' => __( 'Marcas' ),
  ); 
 
// Now register the non-hierarchical taxonomy like tag
 
  register_taxonomy('woo_os_marcas','ordens_de_servico',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_in_rest' => true,
    'show_admin_column' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'topic' ),
  ));
}
//HOOK no itin e cria a taxonomia de marcas para o tipo de post 'ordens_de_servico'
add_action( 'init', 'woo_os_problemas_taxonomy', 0 );
 
function woo_os_problemas_taxonomy() {
 
// Labels part for the GUI
 
  $labels = array(
    'name' => _x( 'Problemas', 'taxonomy general name' ),
    'singular_name' => _x( 'Problema', 'taxonomy singular name' ),
    'search_items' =>  __( 'Pesquisar Problemas' ),
    'popular_items' => __( 'Problemas Populares' ),
    'all_items' => __( 'Todas os Problemas' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Editar Problema' ), 
    'update_item' => __( 'Atualizar Problema' ),
    'add_new_item' => __( 'Adicionar Novo Problema' ),
    'new_item_name' => __( 'Nome da Novo Problema' ),
    'separate_items_with_commas' => __( 'Separe os problemas por vírgula' ),
    'add_or_remove_items' => __( 'Adicione ou remova problemas' ),
    'choose_from_most_used' => __( 'Escolher entre os problemas mais usadas' ),
    'menu_name' => __( 'Problemas' ),
  ); 
 
// Now register the non-hierarchical taxonomy like tag
 
  register_taxonomy('woo_os_problemas','ordens_de_servico',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_in_rest' => true,
    'show_admin_column' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'topic' ),
  ));
}

add_action( 'init', 'woo_os_caracteristicas_taxonomy', 0 );
 
function woo_os_caracteristicas_taxonomy() {
 
// Labels part for the GUI
 
  $labels = array(
    'name' => _x( 'Características', 'taxonomy general name' ),
    'singular_name' => _x( 'Característica', 'taxonomy singular name' ),
    'search_items' =>  __( 'Pesquisar Características' ),
    'popular_items' => __( 'Características Populares' ),
    'all_items' => __( 'Todas as Características' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Editar Característica' ), 
    'update_item' => __( 'Atualizar Característica' ),
    'add_new_item' => __( 'Adicionar Nova Característica' ),
    'new_item_name' => __( 'Nome da Nova Características' ),
    'separate_items_with_commas' => __( 'Separe as características por vírgula' ),
    'add_or_remove_items' => __( 'Adicione ou remova características' ),
    'choose_from_most_used' => __( 'Escolher entre as características mais usadas' ),
    'menu_name' => __( 'Características' ),
  ); 
 
// Now register the non-hierarchical taxonomy like tag
 
  register_taxonomy('woo_os_caracteristicas','ordens_de_servico',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_in_rest' => true,
    'show_admin_column' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'topic' ),
  ));
}

add_action( 'init', 'woo_os_observacoes_taxonomy', 0 );

function woo_os_observacoes_taxonomy() {
 
// Labels part for the GUI
 
  $labels = array(
    'name' => _x( 'Observações', 'taxonomy general name' ),
    'singular_name' => _x( 'Observação', 'taxonomy singular name' ),
    'search_items' =>  __( 'Pesquisar Observações' ),
    'popular_items' => __( 'Observações Populares' ),
    'all_items' => __( 'Todas as Observações' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Editar Observação' ), 
    'update_item' => __( 'Atualizar Observação' ),
    'add_new_item' => __( 'Adicionar Nova Observação' ),
    'new_item_name' => __( 'Nome da Nova Observações' ),
    'separate_items_with_commas' => __( 'Separe as observações por vírgula' ),
    'add_or_remove_items' => __( 'Adicione ou remova observações' ),
    'choose_from_most_used' => __( 'Escolher entre as observações mais usadas' ),
    'menu_name' => __( 'Observações' ),
  ); 
 
// Now register the non-hierarchical taxonomy like tag
 
  register_taxonomy('woo_os_observacoes','ordens_de_servico',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_in_rest' => true,
    'show_admin_column' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'topic' ),
  ));
}

add_filter( 'manage_edit-ordens_de_servico_columns', 'woo_os_remove_tax_column' );

function woo_os_remove_tax_column( $columns ){
    // Change categories for your custom taxonomy
    unset($columns['taxonomy-woo_os_marcas']);
    return $columns;
}

// Add the custom columns to the book post type:

function set_custom_edit_book_columns($columns) {
    unset( $columns['author'] );
    // cliente, marca, entrada, status,
    $columns['woo_os_cliente'] = __( 'Cliente', 'your_text_domain' );
    $columns['woo_os_marca'] = __( 'Marca', 'your_text_domain' );
    $columns['woo_os_entrada'] = __( 'CPF', 'your_text_domain' );
    $columns['woo_os_status'] = __( 'Status', 'your_text_domain' );
    $columns['date'] = __( 'Entrada', 'your_text_domain' );

    return $columns;
}
add_filter( 'manage_ordens_de_servico_posts_columns', 'set_custom_edit_book_columns' );

add_action( 'pre_get_posts', 'extend_admin_search' );
function extend_admin_search( $query ) {
    $post_type = 'ordens_de_servico';
    $custom_fields = array("woo_os_cliente","woo_os_marca","woo_os_user","woo_os_user_cpf", "woo_os_serial", "post_date");
    if( ! is_admin() )
        return;
    if ( $query->query['post_type'] != $post_type )
        return;
    $search_term = $query->query_vars['s'];

    $query->query_vars['s'] = '';

    if ( $search_term != '' ) {
        $meta_query = array( 'relation' => 'OR' );
        foreach( $custom_fields as $custom_field ) {
            array_push( $meta_query, array(
                'key' => $custom_field,
                'value' => $search_term,
                'compare' => 'LIKE'
            ));
        }
        $query->set( 'meta_query', $meta_query );
    };
}

// Add the data to the custom columns for the book post type:
add_action( 'manage_ordens_de_servico_posts_custom_column' , 'custom_book_column', 10, 2 );
function custom_book_column( $column, $post_id ) {
if ( $column == 'woo_os_status' ) {
    $new_status = get_post_meta( $post_id, 'woo_os_status', true);

     switch ($new_status) {
            case 1:
                $os_status_description = "Aguardando Orçamento";
                break;
            case 2:
                $os_status_description = "Aguardando Pagamento";
                break;
            case 3:
                $os_status_description = "Aguardando Material";
                break;
            case 4:
                $os_status_description = "Orçamento Reprovado";
                break;
            case 5:
                $os_status_description = "Em Execução";
                break;
            case 6:
                $os_status_description = "Reprovado pelo Técnico";
                break;
            case 7:
                $os_status_description = "Pronto";
                break;
            case 8:
                $os_status_description = "Concluído";
                break;
            case 9:
                $os_status_description = "Estornado";
                break;
            
            default:
                $os_status_description = "--";
        }
    echo $os_status_description;
  }
if ( $column == 'woo_os_cliente' ) {
    $nome_do_cliente = get_user_by( 'email', get_post_meta( $post_id, 'woo_os_user', true ) );

    // echo get_post_meta( $post_id, 'woo_os_user_email', true );
    echo $nome_do_cliente->first_name .' '.$nome_do_cliente->last_name;
  }
  if ( $column == 'woo_os_marca' ) {
    echo get_post_meta( $post_id, 'woo_os_marca', true );
  }
  if ( $column == 'woo_os_entrada' ) {
    $nome_do_cliente = get_user_by( 'email', get_post_meta( $post_id, 'woo_os_user', true ) );
    echo get_user_meta( $nome_do_cliente->ID, 'billing_cpf', true );
  }
}
/**
 * HTML DA METABOX DO CLIENTE
 */
function woo_os_cliente_data() {
    global $post;
    
    // Nonce para validar o request
    wp_nonce_field( basename( __FILE__ ), 'woo_os__dados' );

    // Output the field
    echo '<fieldset class="campos-identificacao">
                <input type="text" id="woo_os_user_email" name="woo_os_user_email" value="'. get_post_meta($post->ID, 'woo_os_user_email', true) .'" placeholder="digite um email, cpf ou cnpj" required>
                 
                <input class="button-primary" type="button" name="check_user" id="check_user" value="Verificar" /><br>

                <label for="woo_os_user" class="single-line">Email do cliente</label><br>
                <input type="email" id="woo_os_user" name="woo_os_user" value="'. get_post_meta($post->ID, 'woo_os_user', true) .'" required><br>

                <label for="tipo_pessoa" class="single-line">Tipo de pessoa</label><br>
                <select name="billing_persontype" id="billing_persontype" class="single-line">
                    <option value="1">Pessoa Física</option>
                    <option value="2">Pessoa Jurídica</option>
                </select><br>
            </fieldset>';

    echo '<fieldset class="complementos-pj">
            <label for="billing_company" class="single-line">Razão Social</label><br>
            <input type="text" name="billing_company" id="billing_company" value="" class="regular-text single-line" required><br>
            
            <label for="billing_cnpj" class="single-line">Documento</label><br>
            <input type="text" name="billing_cnpj" id="billing_cnpj" class="regular-text single-line" required><br>

            <label for="billing_ie" class="single-line">Inscrição Estadual</label><br>
            <input type="text" name="billing_ie" id="billing_ie" class="regular-text single-line"><br>
        </fieldset>';

    echo '<fieldset class="complementos-pf">
            <label for="billing_first_name" class="single-line">Nome</label><br>
            <input type="text" name="billing_first_name" id="billing_first_name" class="regular-text single-line" required><br>

            <label for="billing_last_name" class="single-line">Sobrenome</label><br>
            <input type="text" name="billing_last_name" id="billing_last_name" class="regular-text single-line" required><br>

            <label for="billing_phone" class="single-line">Telefone</label><br>
            <input type="text" name="billing_phone" id="billing_phone" class="regular-text single-line" required><br>

            <label for="billing_cellphone" class="single-line">Celular</label><br>
            <input type="text" name="billing_cellphone" id="billing_cellphone" class="regular-text single-line" required><br>
            
            <label for="billing_cpf" class="single-line">Documento</label><br>
            <input type="text" name="billing_cpf" id="billing_cpf" class="regular-text single-line" required><br>

            <label for="billing_sex" class="single-line">Gênero</label><br>
            <label title="Masculino">
                <input type="radio" name="billing_sex" value="Masculino" required/>
                <span>Masculino</span>
            </label><br>
            <label title="Feminino">
                <input type="radio" name="billing_sex" value="Feminino"/>
            <span>Feminino</span>
            </label><br>

            <label for="billing_birthdate">Data de nascimento</label><br>
            <input type="text" id="billing_birthdate" name="billing_birthdate" required>
        </fieldset>';

    echo '<fieldset class="complementos-endereco">
            <label for="billing_postcode">CEP</label><br>
            <input type="text" name="billing_postcode" id="billing_postcode" onblur="pesquisacep(this.value);" required/><br>

            <label for="billing_address_1">Endereço</label><br>
            <input type="text" name="billing_address_1" id="billing_address_1" required/><br>

            <label for="billing_number">Número</label><br>
            <input type="text" name="billing_number" id="billing_number" required/><br>

            <label for="billing_address_2">Complemento</label><br>
            <input type="text" name="billing_address_2" id="billing_address_2" /><br>

            <label for="billing_neighborhood">Bairro</label><br>
            <input type="text" name="billing_neighborhood" id="billing_neighborhood" required/><br>

            <label for="billing_city">Cidade</label><br>
            <input type="text" name="billing_city" id="billing_city" required/><br>

            <label for="billing_state">Estado</label><br>
            <input type="text" id="billing_state" name="billing_state" value="" required/>

            <input type="hidden" name="billing_country" id="billing_country" value="BR" required/><br>
        </fieldset>';

}
/**
 * HTML DA METABOX DA OS
 */
function woo_os_os_data() {
    global $post;
    // recupera o usuário atual
    $current_user = wp_get_current_user();
    // mostra os campos
    // $protudos_values_array = $_POST['woo_os_produto'];
    // $marca_values_array = $_POST['woo_os_marca'];
    // $referencia_values_array = $_POST['woo_os_referencia'];
    // $problemas_values_array = $_POST['woo_os_problemas'];

    $protudos_values_array = get_post_meta( get_the_ID(), 'woo_os_produto' );
    $marca_values_array = get_post_meta( get_the_ID(), 'woo_os_marca' );
    $referencia_values_array = get_post_meta( get_the_ID(), 'woo_os_referencia' );
    $problemas_values_array = get_post_meta( get_the_ID(), 'woo_os_problemas' );
    $tipo_os = get_post_meta($post->ID, 'woo_os_tipo', true);
    $na_garantia = get_post_meta($post->ID, 'woo_os_garantia', true);
    
    echo '<fieldset class="campos_os">
    
            <label for="woo_os_tipo">Tipo de OS</label><br>
            <select name="woo_os_tipo" class="single-line" id="woo_os_tipo">
                <option value="1" '. (isset($tipo_os) && $tipo_os == '1' ? 'selected="selected"' : '') .'>Expressa</option>
                <option value="2" '. (isset($tipo_os) && $tipo_os == '2' ? 'selected="selected"' : '') .'>Completa</option>
            </select><br>

            <label for="woo_os_garantia">Garantia</label><br>
            <select name="woo_os_garantia" class="single-line">
                <option value="2" '. (isset($na_garantia) && $na_garantia == '2' ? 'selected="selected"' : '') .'>Não</option>
                <option value="1" '. (isset($na_garantia) && $na_garantia == '1' ? 'selected="selected"' : '') .'>Sim</option>   
            </select><br><div class="field_wrapper">';

       
        if ( $protudos_values_array && is_array($protudos_values_array) ) {
            foreach( $protudos_values_array as $index=>$value ) { ?>
                <div>
                <label for="woo_os_produto">Tipo de Produto</label>
                <?php
                if($index >= 1){
                        
                        echo '<a href="javascript:void(0);" class="remove_button"><span class="dashicons dashicons-remove"></span></a>';
                    }else{
                        echo '<a href="javascript:void(0);" class="add_button" title="Add field"><span class="dashicons dashicons-insert"></span></a>';
                    }
                    ?>
                    <br>
                    <input type="text" name="woo_os_produto[]" class="regular-text single-line" value="<?php echo (isset($value)) ? ($value) : ("Relógio"); ?>" required/><br>

                    <label for="woo_os_marca">Marca</label><br>
                    <!-- <input type="text" name="woo_os_marca[]" class="regular-text single-line" value="<?php echo $marca_values_array[$index]; ?>" required/><br> -->

                    <select name="woo_os_marca[]"  id="woo_os_marca" class="woo_os_marca">
                    <?php
                        $tags = get_tags(array(
                          'taxonomy' => 'woo_os_marcas',
                          'orderby' => 'name',
                          'hide_empty' => false // for development
                        ));
                        $query = new WP_Term_Query(array(
                            'object_ids' =>  get_the_ID(),
                            'fields'     => 'ids',
                        ));

                        $tag_id = get_queried_object()->term_id;
                        if ( $tags ) {
                            foreach ( $tags as $tag ) {
                                $selected = ( $tag->name == get_post_meta($post->ID, 'woo_os_marca', true) )?'selected':'';
                                printf('<option %1$s value="%2$s">%3$s</option>',
                                    $selected,
                                    $tag->name,
                                    $tag->name
                                );    
                            }
                        }
                    ?>
                    </select><br>

                    <label for="woo_os_referencia">Referência</label><br>
                    <input type="text" name="woo_os_referencia[]" class="regular-text single-line" value="<?php echo $referencia_values_array[$index]; ?>" required/><br>

                    <label for="woo_os_problemas">Problema(s) declarado(s)</label><br>
                  <br>
                    <select class="woo_os_problem_search" id="woo_os_problem_search" name="woo_os_problemas[]" multiple="multiple" style="width:100%">
                        <?php
                        $tags = get_tags(array(
                          'taxonomy' => 'woo_os_problemas',
                          'orderby' => 'name',
                          'hide_empty' => false // for development
                        ));
                        $query = new WP_Term_Query(array(
                            'object_ids' =>  get_the_ID(),
                            'fields'     => 'ids',
                        ));

                        $tag_id = get_queried_object()->term_id;
                        if ( $tags ) {
                            foreach ( $tags as $tag ) {
                                $selected = ( $tag->name == get_post_meta($post->ID, 'woo_os_marca', true) )?'selected':'';
                                printf('<option %1$s value="%2$s">%3$s</option>',
                                    $selected,
                                    $tag->name,
                                    $tag->name
                                );    
                            }
                        }
                    ?>
                    </select><br>
                </div>
                    <?php
                }
        }else{
            echo '
                <div>
                    <label for="woo_os_produto">Tipo de Produto</label><a href="javascript:void(0);" class="add_button" title="Add field"><span class="dashicons dashicons-insert"></span></a><br>
                    <input type="text" name="woo_os_produto[]" class="regular-text single-line" value="'; echo (get_post_meta($post->ID, 'woo_os_produto', true) ) ? ($value) : ("Relógio");   echo '"/><br>

                    <label for="woo_os_marca">Marca</label><br>
                    <select name="woo_os_marca[]"  id="woo_os_marca" class="woo_os_marca">';
                   
                        $tags = get_tags(array(
                          'taxonomy' => 'woo_os_marcas',
                          'orderby' => 'name',
                          'hide_empty' => false // for development
                        ));
                        $query = new WP_Term_Query(array(
                            'object_ids' =>  get_the_ID(),
                            'fields'     => 'ids',
                        ));

                        $tag_id = get_queried_object()->term_id;
                        if ( $tags ) {
                            foreach ( $tags as $tag ) {
                                $selected = ( $tag->name == get_post_meta($post->ID, 'woo_os_marca', true) )?'selected':'';
                                printf('<option %1$s value="%2$s">%3$s</option>',
                                    $selected,
                                    $tag->name,
                                    $tag->name
                                );    
                            }
                        }
                    
                   echo '</select><br>

                    <label for="woo_os_referencia">Referência</label><br>
                    <input type="text" name="woo_os_referencia[]" class="regular-text single-line" value="'  . get_post_meta($post->ID, 'woo_os_referencia', true)  . '" required/><br>

                     <label for="woo_os_problemas">Problema(s) declarado(s)</label><br>
                    <textarea name="woo_os_problemas[]" id="woo_os_problemas" class="widefat" rows="4" required>'  . get_post_meta($post->ID, 'woo_os_problemas', true)  . '</textarea><br>
                </div>
            </div>';                
        }
        // echo '</div>';
        echo '</fieldset>';

        echo '<fieldset class="complementos-os">
            <label for="woo_os_caracteristicas">Características</label><br>
            <input type="text" name="woo_os_caracteristicas" id="woo_os_caracteristicas" class="regular-text single-line" value="'  . get_post_meta($post->ID, 'woo_os_caracteristicas', true)  . '" required/><br>

            <label for="woo_os_ext_obs">Observações externas</label><br>
            <textarea name="woo_os_ext_obs" id="woo_os_ext_obs" class="widefat" rows="4" required>'  . get_post_meta($post->ID, 'woo_os_ext_obs', true)  . '</textarea><br></fieldset>';

          /*  <label for="woo_os_mecanismo">Mecanismo</label><br>
                <input type="text" name="woo_os_mecanismo" id="woo_os_mecanismo" class="regular-text single-line" value="'  . get_post_meta($post->ID, 'woo_os_mecanismo', true)  . '"/><br>
            
        */

}
// HTML DOS CAMPOS DO ORCAMENTO
function woo_os_orcamento(){
    global $post;

    echo '<fieldset class="complementos-os">
            <label for="woo_os_calibre">Calibre</label><br>
                <input type="text" name="woo_os_calibre" id="woo_os_calibre" class="single-line" value="'  . get_post_meta($post->ID, 'woo_os_calibre', true)  . '"/><br>

                <label for="woo_os_validade">Validade</label><br>
                <input type="number" name="woo_os_validade" id="woo_os_validade" min="1" class="single-line" value="'  . get_post_meta($post->ID, 'woo_os_validade', true)  . '"/><br>

                <label for="woo_os_tempo_servico">Tempo de Serviço </label><br>
                <input type="number" name="woo_os_tempo_servico" id="woo_os_tempo_servico" min="1" class="single-line" value="'  . get_post_meta($post->ID, 'woo_os_tempo_servico', true)  . '"/><br>

                <label for="woo_os_previsao_entrega">Previsão de entrega (dias)</label><br>
                <input type="number" name="woo_os_previsao_entrega" id="woo_os_previsao_entrega" min="1" class="single-line" value="'  . get_post_meta($post->ID, 'woo_os_previsao_entrega', true)  . '"/><br>

                <label for="woo_os_tempo_garantia">Meses de garantia</label><br>
                <input type="number" name="woo_os_tempo_garantia" id="woo_os_tempo_garantia" min="0" max="12" class="single-line" value="'  . get_post_meta($post->ID, 'woo_os_tempo_garantia', true)  . '"/><br>

                <label for="woo_os_observacao_interna">Observação interna</label><br>
                <textarea name="woo_os_observacao_interna" id="woo_os_observacao_interna" class="widefat" rows="4">'  . get_post_meta($post->ID, 'woo_os_observacao_interna', true)  . '</textarea><br>

                <label for="woo_os_orcamentista">Orçamentista</label><br>
                <input type="text" name="woo_os_orcamentista" id="woo_os_orcamentista" class="single-line" value="'  . get_post_meta($post->ID, 'woo_os_orcamentista', true)  . '"/><br> 
            </fieldset>';
}
// recupera o último nr de série de OS gravado no sistema.
function get_min_max_meta_value( $type = 'max', $key = 'woo_os_serial' ){

    global $wpdb;
    $cash_key = md5($key . $type);
    $results = wp_cache_get($cash_key);

    if($results === false){

        $sql = "SELECT " . $type . "( cast( meta_value as UNSIGNED ) ) FROM {$wpdb->postmeta} WHERE meta_key='%s'";
        $query = $wpdb->prepare( $sql, $key);

        $results = $wpdb->get_var( $query );
        
        return $results;
    }
    return $results;
}
/**
 * Dados do pedido
 */
function woo_os_order_data() {
    global $post;
    //mais alguns dados necessarios para preencher inputs select
    $status = get_post_meta($post->ID, 'woo_os_status', true);
    // $tipo_os = get_post_meta($post->ID, 'woo_os_tipo', true);
    // $na_garantia = get_post_meta($post->ID, 'woo_os_garantia', true);
    $pedido_vinculado =  get_post_meta($post->ID, 'os_pedido_criado', true);

    if(get_option( 'wt-order-number' ) && get_option('wt-order-number') == 'on'){
        $wt_order_pedido_vinculado  =  get_post_meta($pedido_vinculado, '_order_number', true);  
    }                         
    // define o nr de série à partir de 1000 quando não houver um nr de série vinculado
    // ou define o nr de série sequencial à partir do último nr de série +1
    if(!get_post_meta($post->ID, 'woo_os_serial', true)){
        $nr_serial_os = get_min_max_meta_value();

        if($nr_serial_os == "" || !isset($nr_serial_os)){
            $nr_serial_os = '1000';
        }else{
            $nr_serial_os++;
        }
    }else{
        $nr_serial_os = get_post_meta($post->ID, 'woo_os_serial', true);
        // echo 'serial posted: '. $nr_serial_os;
    }
    
    // mostra os campos
    echo '<fieldset class="numeracao-os">
            <label for="woo_os_serial">Numero da OS</label><br>
            <input type="number" name="woo_os_serial" id="woo_os_serial" class="" value="'  . $nr_serial_os . '" readonly="true"/><br>
            <input type="hidden" name="woo_os_number" id="woo_os_number" class="" value="'  . get_the_ID()  . '" readonly="true"/>

            <label for="woo_os_valor">Valor</label><br>
            <input type="text" name="woo_os_valor" id="woo_os_valor" class="" value="'  . get_post_meta($post->ID, 'woo_os_valor', true)  . '" readonly="true"/><br>

            <label for="woo_os_status" class="single-line">Status da OS</label><br>
            <select name="woo_os_status" class="single-line" id="woo_os_status">
                <option value="1" '. (isset($status) && $status == '1' ? 'selected="selected"' : '') .'>Aguardando Orçamento</option>
                <option value="2" '. (isset($status) && $status == '2' ? 'selected="selected"' : '') .'>Aguardando Pagamento</option>
                <option value="3" '. (isset($status) && $status == '3' ? 'selected="selected"' : '') .'>Aguardando Material</option>
                <option value="4" '. (isset($status) && $status == '4' ? 'selected="selected"' : '') .'>Orçamento Reprovado</option>
                <option value="5" '. (isset($status) && $status == '5' ? 'selected="selected"' : '') .'>Em Execução</option>
                <option value="6" '. (isset($status) && $status == '6' ? 'selected="selected"' : '') .'>Reprovado pelo Técnico</option>
                <option value="7" '. (isset($status) && $status == '7' ? 'selected="selected"' : '') .'>Pronto</option>
                <option value="8" '. (isset($status) && $status == '8' ? 'selected="selected"' : '') .'>Concluído</option>
                <option value="9" '. (isset($status) && $status == '9' ? 'selected="selected"' : '') .'>Estornado</option>
            </select><br>

            <label for="os_pedido_criado">Pedido vinculado</label><br>'.
                (isset($wt_order_pedido_vinculado) ? 
                    '<input type="hidden" name="os_pedido_criado" id="os_pedido_criado" class="" value="'  . $pedido_vinculado  . '"readonly="true"/>
                    <input type="text" name=wt_order_pedido_vinculado" id="wt_order_pedido_vinculado" class="" value="'  . $wt_order_pedido_vinculado  . '" readonly="true"/><br>'
                    : '<input type="text" name="os_pedido_criado" id="os_pedido_criado" class="" value="'  . $pedido_vinculado  . '" readonly="true"/><br>');
            if(empty($pedido_vinculado)){
                echo '<label for="criar_pedido">Criar um novo pedido</label><br>
                            <input class="button-secondary" type="button" name="criar_pedido" id="criar_pedido" value="Criar Pedido" /><br>';
            }else{
                echo '<label for="ver_pedido">Ver pedido</label><br>
                            <input class="button-secondary" type="button" name="ver_pedido" id="ver_pedido" value="Ver Pedido" /><br>';
            }
            

        echo   '<label for="criar_pedido">Imprimir a OS</label><br>
            <input class="button-secondary" type="button" name="imprimir_os" id="imprimir_os" value="Imprimir" /><br>
        </fieldset>';

}
// remove os dados de produtos repetidos que foram apagados
function delete_all_old_meta($post_id){
    // apaga toda a meta para impedir dados duplicados
    delete_post_meta( $post_id, 'woo_os_produto' );
    delete_post_meta( $post_id, 'woo_os_marca' );
    delete_post_meta( $post_id, 'woo_os_referencia' );
    delete_post_meta( $post_id, 'woo_os_problemas' );
}
/**
 * SALVA OS DADOS
 */
function strtoupper_utf8($string){
    $string=utf8_decode($string);
    $string=strtoupper($string);
    $string=utf8_encode($string);
    return $string;
}

function wpt_os_save__meta( $post_id, $post ) {

    global $wpdb, $post;
    // Cancela a edição se o usuário não tier a permissão correta
    if ( ! current_user_can( 'edit_post', $post_id ) ) {
        return $post_id;
    }

    // VERIFICANDO SE O NONCE EXISTE ANTES DE POSTAR
    if (isset($woo_os_meta['woo_os_user_email']) || !wp_verify_nonce( $_POST['woo_os__dados'], basename(__FILE__) ) ) {
        return $post_id;
    }
    // apaga toda a meta para impedir dados duplicados
   

    $woo_os_meta['woo_os_user'] = sanitize_text_field( $_POST['woo_os_user']);
    // SATINIZANDO OS CAMPOS PQ NÂO DA PRA CONFIAR NO USUARIO    
    $woo_os_meta['woo_os_user_email'] = sanitize_text_field( $_POST['woo_os_user_email']);
    $woo_os_meta['woo_os_user_cpf'] = sanitize_text_field( $_POST['billing_cpf']);
    $woo_os_meta['woo_os_status'] = sanitize_text_field( $_POST['woo_os_status']);
    $woo_os_meta['woo_os_tipo'] = esc_textarea( $_POST['woo_os_tipo']);

    $woo_os_meta['woo_os_serial'] = esc_textarea( $_POST['woo_os_serial']);
    $woo_os_meta['woo_os_garantia'] = esc_textarea( $_POST['woo_os_garantia']);

    // campos repetíveis
    $woo_os_product_meta_os_produto = $_POST['woo_os_produto'];
    $woo_os_product_meta_os_marca = $_POST['woo_os_marca'];
    $woo_os_product_meta_os_referencia = $_POST['woo_os_referencia'];
    $woo_os_product_meta_os_problemas = $_POST['woo_os_problemas'];

    $woo_os_meta['woo_os_caracteristicas'] = esc_textarea( $_POST['woo_os_caracteristicas']);
    $woo_os_meta['woo_os_ext_obs'] = sanitize_textarea_field( $_POST['woo_os_ext_obs']);
    // $woo_os_meta['woo_os_atendente'] = esc_textarea( $_POST['woo_os_atendente']);
    // $woo_os_meta['woo_os_mecanismo'] = esc_textarea( $_POST['woo_os_mecanismo']);
    $woo_os_meta['woo_os_calibre'] = esc_textarea( $_POST['woo_os_calibre']);
    // $woo_os_meta['woo_os_valor'] = esc_textarea( $_POST['woo_os_valor']);
    $woo_os_meta['woo_os_validade'] = esc_textarea( $_POST['woo_os_validade']);
    $woo_os_meta['woo_os_tempo_servico'] = esc_textarea( $_POST['woo_os_tempo_servico']);
    $woo_os_meta['woo_os_previsao_entrega'] = esc_textarea( $_POST['woo_os_previsao_entrega']);
    $woo_os_meta['woo_os_tempo_garantia'] = esc_textarea( $_POST['woo_os_tempo_garantia']);
    $woo_os_meta['woo_os_observacao_interna'] = sanitize_textarea_field( $_POST['woo_os_observacao_interna']);
    $woo_os_meta['woo_os_orcamentista'] = esc_textarea( $_POST['woo_os_orcamentista']);

    $woo_os_user_meta['woo_os_user'] = sanitize_text_field( $_POST['woo_os_user']);
    // SATINIZANDO OS CAMPOS PQ NÂO DA PRA CONFIAR NO USUARIO    
    $woo_os_user_meta['billing_persontype'] = esc_textarea( $_POST['billing_persontype']);
    $woo_os_user_meta['billing_first_name'] = sanitize_text_field( $_POST['billing_first_name']);
    $woo_os_user_meta['billing_last_name'] = esc_textarea( $_POST['billing_last_name']);
    $woo_os_user_meta['billing_phone'] = sanitize_text_field( $_POST['billing_phone']);
    $woo_os_user_meta['billing_cellphone'] = sanitize_text_field( $_POST['billing_cellphone']);

    if($woo_os_user_meta['billing_persontype'] == "1"){
        $woo_os_user_meta['billing_cpf'] = esc_textarea( $_POST['billing_cpf']);
    }
    
    if($woo_os_user_meta['billing_persontype'] == "2"){
        $woo_os_user_meta['billing_cnpj'] = esc_textarea( $_POST['billing_cnpj']);
        $woo_os_user_meta['billing_ie'] = esc_textarea( $_POST['billing_ie']);
        $woo_os_user_meta['billing_company'] = esc_textarea( $_POST['billing_company']);
    }

    $woo_os_user_meta['billing_sex'] = esc_textarea( $_POST['billing_sex']);
    $woo_os_user_meta['billing_birthdate'] = esc_textarea( $_POST['billing_birthdate']);
    $woo_os_user_meta['billing_postcode'] = esc_textarea( $_POST['billing_postcode']);
    $woo_os_user_meta['billing_address_1'] = esc_textarea( $_POST['billing_address_1']);
    $woo_os_user_meta['billing_number'] = sanitize_textarea_field( $_POST['billing_number']);
    $woo_os_user_meta['billing_address_2'] = sanitize_textarea_field( $_POST['billing_address_2']);
    $woo_os_user_meta['billing_neighborhood'] = esc_textarea( $_POST['billing_neighborhood']);
    $woo_os_user_meta['billing_city'] = esc_textarea( $_POST['billing_city']);
    $woo_os_user_meta['billing_state'] = esc_textarea( $_POST['billing_state']);
    $woo_os_user_meta['billing_country'] = esc_textarea( $_POST['billing_country']);

    // verifica se o usuário existe ou cria um novo usuário
    if( !email_exists( $woo_os_meta['woo_os_user'] ) ){

        $userdata = array(
            'user_pass'             => NULL,   //(string) The plain-text user password.
            'user_login'            => $_POST['woo_os_user'],   //(string) The user's login username.
            'user_nicename'         => $_POST['billing_first_name'],   //(string) The URL-friendly user name.
            'user_email'            => $_POST['woo_os_user'],   //(string) The user email address.
            'first_name'            => $_POST['billing_first_name'],   //(string) The user's first name. For new users, will be used to build the first part of the user's display name if $display_name is not specified.
            'last_name'             => $_POST['billing_last_name'],   //(string) The user's last name. For new users, will be used to build the second part of the user's display name if $display_name is not specified.
            'role'                  => 'customer',   //(string) User's role.      
        );
         
        $user_id = wp_insert_user( $userdata ) ;
         
        // On success.
        if ( ! is_wp_error( $user_id ) ) {
            echo "User created : ". $user_id;
        }

        $os_user = get_user_by( 'email', $_POST['woo_os_user'] );
        foreach($woo_os_user_meta as $key => $value) {
                update_user_meta( $user_id, $key, $value );
        } 

    }else{
        // atualiza os metadados do usuário caso ele exista
        $os_user = get_user_by( 'email', $_POST['woo_os_user'] );
        foreach($woo_os_user_meta as $key => $value) {
            // update_user_meta( $os_user->user_id, $key, $value );

            if ( get_user_meta( $os_user->ID, $key, false ) ) {
                    //SE JA TIVER PREENCHIDO ATUALIZA
                update_user_meta( $os_user->ID, $key, $value );
            } else {
                // SE ESTIVER EM BRANCO CRIA
                add_user_meta( $os_user->ID, $key, $value);
            }
        } 
    }

    // META - FAZ O TRABALHO DURO DE PUBLICAR O CONTEUDO
    foreach ( $woo_os_meta as $key => $value ) {

        // Don't store custom data twice
        if ( 'revision' === $post->post_type ) {
            return;
        }

        if ( get_post_meta( $post_id, $key, false ) === false ) {
            //Se a variável não estiver corretamente definida
            return;
        }

        if ( get_post_meta( $post_id, $key, false ) ) {
            //SE JA TIVER PREENCHIDO ATUALIZA
            update_post_meta( $post_id, $key, strtoupper_utf8($value) );
        } else {
            // SE ESTIVER EM BRANCO CRIA
            add_post_meta( $post_id, $key, strtoupper_utf8($value) );
        }
        if ( ! $value ) {
            // SE APAGAR O CAMPO... APAGA
            delete_post_meta( $post_id, $key );
        }

    }
    delete_all_old_meta($post_id);
    
    foreach($woo_os_product_meta_os_produto as $value){
        add_post_meta( $post_id, 'woo_os_produto', strtoupper_utf8($value));
    }
    foreach($woo_os_product_meta_os_marca as $value){
        add_post_meta( $post_id, 'woo_os_marca', strtoupper_utf8($value));
    }
    foreach($woo_os_product_meta_os_referencia as $value){
        add_post_meta( $post_id, 'woo_os_referencia', strtoupper_utf8($value));
    }
    foreach($woo_os_product_meta_os_problemas as $value){
        add_post_meta( $post_id, 'woo_os_problemas', strtoupper_utf8($value));
    }

    // atualiza o nome do post com o nr da OS
    if(the_title() == ''){
        $title = get_post_meta( $post_id, 'woo_os_serial', true );
        $where = array( 'ID' => $post_id );
        $wpdb->update( $wpdb->posts, array( 'post_title' => $title ), $where );
    }

}
add_action( 'save_post', 'wpt_os_save__meta', 1, 2 );

?>