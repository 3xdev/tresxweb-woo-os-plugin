<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

function is_cpf( $cpf ) {
		$cpf = preg_replace( '/[^0-9]/', '', $cpf );

		if ( 11 !== strlen( $cpf ) || preg_match( '/^([0-9])\1+$/', $cpf ) ) {
			return false;
		}

		$digit = substr( $cpf, 0, 9 );

		for ( $j = 10; $j <= 11; $j++ ) {
			$sum = 0;

			for ( $i = 0; $i < $j - 1; $i++ ) {
				$sum += ( $j - $i ) * intval( $digit[ $i ] );
			}

			$summod11 = $sum % 11;
			$digit[ $j - 1 ] = $summod11 < 2 ? 0 : 11 - $summod11;
		}

		return intval( $digit[9] ) === intval( $cpf[9] ) && intval( $digit[10] ) === intval( $cpf[10] );
	}

function is_cnpj( $cnpj ) {
	$cnpj = sprintf( '%014s', preg_replace( '{\D}', '', $cnpj ) );

	if ( 14 !== strlen( $cnpj ) || 0 === intval( substr( $cnpj, -4 ) ) ) {
		return false;
	}

	for ( $t = 11; $t < 13; ) {
		for ( $d = 0, $p = 2, $c = $t; $c >= 0; $c--, ( $p < 9 ) ? $p++ : $p = 2 ) {
			$d += $cnpj[ $c ] * $p;
		}

		if ( intval( $cnpj[ ++$t ] ) !== ( $d = ( ( 10 * $d ) % 11 ) % 10 ) ) {
			return false;
		}
	}

	return true;
}
function formatar_cpf_cnpj($doc) {
 
    $doc = preg_replace("/[^0-9]/", "", $doc);
    $qtd = strlen($doc);

    if($qtd >= 11) {

        if($qtd === 11 ) {

            $docFormatado = substr($doc, 0, 3) . '.' .
                            substr($doc, 3, 3) . '.' .
                            substr($doc, 6, 3) . '-' .
                            substr($doc, 9, 2);
        } else {
            $docFormatado = substr($doc, 0, 2) . '.' .
                            substr($doc, 2, 3) . '.' .
                            substr($doc, 5, 3) . '/' .
                            substr($doc, 8, 4) . '-' .
                            substr($doc, -2);
        }

        return $docFormatado;

    } else {
        return "false";
    }
}

add_action( 'wp_ajax_my_action', 'my_action' );

function my_action() {
	
		$identificador_cliente =  $_POST['email_do_cliente'];

		if( is_email($identificador_cliente) ){

			$user = get_user_by( 'email', $identificador_cliente );
			
			$user_data=get_user_meta($user->ID);
			$user_data['woo_user_email'] = $user->user_email;

			echo json_encode($user_data);

		} elseif( is_cpf($identificador_cliente) ) {

			$key = formatar_cpf_cnpj($identificador_cliente);
			$user = get_users( array('meta_key' => 'billing_cpf', 'meta_value' => $key) );

			if($user){
				$user_data = get_user_meta($user[0]->ID);
				$user_data['woo_user_email'] = $user[0]->user_email;

				echo json_encode($user_data);
			}else{
				echo "false";
			}
			

		} elseif( is_cnpj($identificador_cliente) ) {

			$key = formatar_cpf_cnpj($identificador_cliente);
			$user = get_users( array( 'meta_key' => 'billing_cnpj', 'meta_value' => $key) );

			if($user){
				$user_data=get_user_meta($user[0]->ID);
				$user_data['woo_user_email'] = $user[0]->user_email;

				echo json_encode($user_data);
			}else{
				echo "false";
			}
			
		} else{
			// $user = "false";
			echo "false";
		}

	wp_die(); // this is required to terminate immediately and return a proper response
}


add_action('wp_ajax_create_os_order', 'create_os_order');

function create_os_order() {
	
	global $woocommerce;
  // Get the user ID from an Order ID
	$user = get_user_by( 'email', $_POST['woo_os_user'] );
	
	// to do criar uma opção para configura se o valor da OS vai para o pedido	
	$valor_da_os =  $_POST['valor_da_os'];

	$id_da_os =  $_POST['id_da_os'];

	$serial_da_os = $_POST['nr_serie_os'];
	// Verifica se a OS já está publicada antes de criar um pedido
	if ( 'publish' !== get_post_status( $id_da_os ) ){
		echo "false";
		wp_die();
	}
	// verifica se já existe um pedido vinculado à esta OS
	if ( get_post_meta( $id_da_os, 'os_pedido_criado', false ) ) {
        //SE JA TIVER PREENCHIDO ATUALIZA
        echo "pedido";
		die();
        // update_post_meta( $id_da_os, 'os_pedido_criado', 1 );
    } else {
        // SE ESTIVER EM BRANCO CRIA
        // Get an instance of the WC_Customer Object from the user ID
	$customer = new WC_Customer( $user->ID );

	$username     = $customer->get_username(); // Get username
	$user_email   = $customer->get_email(); // Get account email
	$first_name   = $customer->get_first_name();
	$last_name    = $customer->get_last_name();
	$display_name = $customer->get_display_name();

	// Customer billing information details (from account)
	$billing_first_name = $customer->get_billing_first_name();
	$billing_last_name  = $customer->get_billing_last_name();
	$billing_company    = $customer->get_billing_company();
	$billing_email    = $customer->get_billing_email();
	$billing_phone    = $customer->get_billing_phone();
	$billing_address_1  = $customer->get_billing_address_1();
	$billing_address_2  = $customer->get_billing_address_2();
	$billing_city       = $customer->get_billing_city();
	$billing_state      = $customer->get_billing_state();
	$billing_postcode   = $customer->get_billing_postcode();
	$billing_country    = $customer->get_billing_country();

	$address = array(
	  'first_name' => $billing_first_name,
	  'last_name'  => $billing_last_name,
	  'company'    => $billing_company,
	  'email'      => $billing_email,
	  'phone'      => $billing_phone ,
	  'address_1'  => $billing_address_1,
	  'address_2'  => $billing_address_2,
	  'city'       => $billing_city,
	  'state'      => $billing_state,
	  'postcode'   => $billing_postcode,
	  'country'    => 'BR'
	);

	// Now we create the order
	$order = wc_create_order(array('customer_id' => $user->ID));

	// The add_product() function below is located in /plugins/woocommerce/includes/abstracts/abstract_wc_order.php
	// to do no futuro ativar novamente para alimentar a OS, alimentado por uma opção que pega o id do pedido 
	// $order->add_product(  get_product('90'), 1, [
	//     'subtotal'     => $valor_da_os, // e.g. 32.95
	//     'total'        => $valor_da_os, // e.g. 32.95
	// ] );


	$order->set_address( $address, 'billing' );
	//
	$order->calculate_totals();
	// pega o id do pedido para vincular a OS
	// alterado para get_id
	$order_id = trim( str_replace('#', '', $order->get_id() ) );
	// Sanitize user input  and update the meta field in the database.
    update_post_meta( $order_id, '_woo_os_importada', $id_da_os );
    update_post_meta( $order_id, '_woo_os_serial', $serial_da_os );

    update_post_meta( $order_id, '_billing_persontype',  get_user_meta($user->ID, 'billing_persontype', true));
    update_post_meta( $order_id, '_billing_cpf',  get_user_meta($user->ID, 'billing_cpf', true));
    update_post_meta( $order_id, '_billing_cnpj',  get_user_meta($user->ID, 'billing_cnpj', true));
    update_post_meta( $order_id, '_billing_ie',  get_user_meta($user->ID, 'billing_ie', true));

    update_post_meta( $order_id, '_billing_number',  get_user_meta($user->ID, 'billing_number', true));

    update_post_meta( $order_id, '_billing_birthdate',  get_user_meta($user->ID, 'billing_birthdate', true));
	update_post_meta( $order_id, '_billing_sex', get_user_meta($user->ID, 'billing_sex', true));
	update_post_meta( $order_id, '_billing_cellphone', get_user_meta($user->ID, 'billing_cellphone', true));

	update_post_meta( $order_id, '_billing_number',  get_user_meta($user->ID, 'billing_number', true));
	update_post_meta( $order_id, '_billing_neighborhood',  get_user_meta($user->ID, 'billing_neighborhood', true));
	update_post_meta( $order_id, '_billing_address_2',  get_user_meta($user->ID, 'billing_address_2', true));

	$order->save();
	add_post_meta( $id_da_os, 'os_pedido_criado', $order_id );
	update_post_meta( $id_da_os, 'woo_os_status', '2' );
	// $order->update_status("Completed", 'Imported order', TRUE); 
	echo $order_id; 

	wp_die(); // this is required to terminate immediately and return a proper response
        
    }

	

} 

add_action('wp_ajax_imprimir_ordem_de_servico', 'imprimir_ordem_de_servico');

add_action('wp_ajax_pesquisar_ordem_de_servico', 'pesquisar_ordem_de_servico');
add_action('wp_ajax_nopriv_pesquisar_ordem_de_servico', 'pesquisar_ordem_de_servico');

function pesquisar_ordem_de_servico() {
	
	global $post;
	$id_da_os = $_POST['id_da_os'];

	$user_doc = $_POST['doc_do_cliente'];

	$second_loop = get_posts( array(
	    'meta_key'   => 'woo_os_serial',
	    'meta_value' => $id_da_os,
	    'post_type'   => 'ordens_de_servico'
	) );

	if($second_loop){
		$id_da_os = $second_loop[0]->ID;
		// Verifica se a OS já está publicada antes de imprimir
		if ( 'publish' !== get_post_status( $id_da_os ) ){
			echo "false";
			wp_die();
		}
	}else{
		echo "false";
		wp_die();
	}
	
	if( is_cpf($user_doc) ) {

		$key = formatar_cpf_cnpj($user_doc);
		$user = get_users( array('meta_key' => 'billing_cpf', 'meta_value' => $key) );
		

		if($user){
			$id_user = $user[0]->ID;
		}
		

	} elseif( is_cnpj($user_doc) ) {

		$key = formatar_cpf_cnpj($user_doc);
		$user = get_users( array( 'meta_key' => 'billing_cnpj', 'meta_value' => $key) );

		if($user){
			$id_user = $user[0]->ID;
		}
		
	} else{
		// $user = "false";
		echo "no-customer";
		wp_die();
	}
	
	$dados_da_os = get_post_meta( $id_da_os );

	$dados_da_os['nome_loja']  = get_bloginfo( 'name' );
	$dados_da_os['site_url']  = get_bloginfo( 'url' );
	$dados_da_os['admin_email']  = get_bloginfo( 'admin_email' );
	$dados_da_os['endereco']  = get_option( 'woocommerce_store_address' );
	$dados_da_os['complemento']   = get_option( 'woocommerce_store_address_2' );
	$dados_da_os['cidade']        = get_option( 'woocommerce_store_city' );
	$dados_da_os['cep']    = get_option( 'woocommerce_store_postcode' );

	// dados dos serviços
	$dados_da_os['data_os']  = get_the_date('',$id_da_os);

	// dados do cliente
	$customer = new WC_Customer( $id_user );

	$dados_da_os['billing_persontype'] = get_user_meta( $id_user, 'billing_persontype', true );

	if($dados_da_os['billing_persontype'] == 1){

		$dados_da_os['documento'] = get_user_meta( $id_user, 'billing_cpf', true );
	}
	if($dados_da_os['billing_persontype'] == 2){

		$dados_da_os['documento'] = get_user_meta( $id_user, 'billing_cnpj', true );
	}
	
	$dados_da_os['billing_first_name'] = $customer->get_billing_first_name();
	$dados_da_os['billing_last_name']  = $customer->get_billing_last_name();
	$dados_da_os['billing_company']    = $customer->get_billing_company();
	$dados_da_os['billing_email']    = $customer->get_billing_email();
	$dados_da_os['billing_phone']    = $customer->get_billing_phone();
	$dados_da_os['billing_address_1']  = $customer->get_billing_address_1();
	$dados_da_os['billing_number'] = get_user_meta( $id_user, 'billing_number', true );
	$dados_da_os['billing_address_2']  = $customer->get_billing_address_2();
	$dados_da_os['billing_city']       = $customer->get_billing_city();
	$dados_da_os['billing_state']      = $customer->get_billing_state();
	$dados_da_os['billing_postcode']   = $customer->get_billing_postcode();
	$dados_da_os['billing_country']    = $customer->get_billing_country();

	echo json_encode($dados_da_os);

	wp_die(); // this is required to terminate immediately and return a proper response

} 
function imprimir_ordem_de_servico() {
	
	global $post;
	$id_da_os = $_POST['id_da_os'];
	$user = get_user_by( 'email', $_POST['email_do_cliente'] );

	// Verifica se a OS já está publicada antes de imprimir
	if ( 'publish' !== get_post_status( $id_da_os ) ){
		echo "false";
		wp_die();
	}

	
	
	$dados_da_os = get_post_meta( $id_da_os );

	$dados_da_os['nome_loja']  = get_bloginfo( 'name' );
	$dados_da_os['site_url']  = get_bloginfo( 'url' );
	$dados_da_os['admin_email']  = get_bloginfo( 'admin_email' );
	$dados_da_os['endereco']  = get_option( 'woocommerce_store_address' );
	$dados_da_os['complemento']   = get_option( 'woocommerce_store_address_2' );
	$dados_da_os['cidade']        = get_option( 'woocommerce_store_city' );
	$dados_da_os['cep']    = get_option( 'woocommerce_store_postcode' );

	// dados dos serviços
	$dados_da_os['data_os']  = get_the_date('',$id_da_os);

	// dados do cliente
	$customer = new WC_Customer( $user->ID );

	$dados_da_os['billing_persontype'] = get_user_meta( $user->ID, 'billing_persontype', true );

	if($dados_da_os['billing_persontype'] == 1){

		$dados_da_os['documento'] = get_user_meta( $user->ID, 'billing_cpf', true );
	}
	if($dados_da_os['billing_persontype'] == 2){

		$dados_da_os['documento'] = get_user_meta( $user->ID, 'billing_cnpj', true );
	}
	
	$dados_da_os['billing_first_name'] = $customer->get_billing_first_name();
	$dados_da_os['billing_last_name']  = $customer->get_billing_last_name();
	$dados_da_os['billing_company']    = $customer->get_billing_company();
	$dados_da_os['billing_email']    = $customer->get_billing_email();
	$dados_da_os['billing_phone']    = $customer->get_billing_phone();
	$dados_da_os['billing_address_1']  = $customer->get_billing_address_1();
	$dados_da_os['billing_number'] = get_user_meta( $user->ID, 'billing_number', true );
	$dados_da_os['billing_address_2']  = $customer->get_billing_address_2();
	$dados_da_os['billing_city']       = $customer->get_billing_city();
	$dados_da_os['billing_state']      = $customer->get_billing_state();
	$dados_da_os['billing_postcode']   = $customer->get_billing_postcode();
	$dados_da_os['billing_country']    = $customer->get_billing_country();

	echo json_encode($dados_da_os);

	wp_die(); // this is required to terminate immediately and return a proper response

}  

add_action( 'wp_ajax_search_for_os', 'woo_os_get_posts_ajax_callback' ); // wp_ajax_{action}
function woo_os_get_posts_ajax_callback(){

	// we will pass post IDs and titles to this array
	$return = array();

	// you can use WP_Query, query_posts() or get_posts() here - it doesn't matter
	$search_results = new WP_Query( array( 
		's'=> $_GET['q'], // the search query
		'post_status' => 'publish', // if you don't want drafts to be returned
		'ignore_sticky_posts' => 1,
		'posts_per_page' => 50, // how much to show at once
		'post_type' => 'ordens_de_servico',
		'meta_query' => array(
	        'os_clause' => array(
	            'key' => 'os_pedido_criado',
	            'compare' => 'NOT EXISTS',
	        ), 
	    ),
	) );
	if( $search_results->have_posts() ) :
		while( $search_results->have_posts() ) : $search_results->the_post();	
			// shorten the title a little
			$title = ( mb_strlen( $search_results->post->post_title ) > 50 ) ? mb_substr( $search_results->post->post_title, 0, 49 ) . '...' : $search_results->post->post_title;
			$return[] = array( $search_results->post->ID, $title ); // array( Post ID, Post Title )
		endwhile;
	endif;
	echo json_encode( $return );
	die;
}
add_action( 'wp_ajax_search_for_os_problem', 'woo_os_get_problems_ajax_callback' ); // wp_ajax_{action}
function woo_os_get_problems_ajax_callback(){

	// we will pass post IDs and titles to this array
	$return = array();

	// you can use WP_Query, query_posts() or get_posts() here - it doesn't matter
	$search_results = new WP_Query( array( 
		's'=> $_GET['q'], // the search query
		'post_status' => 'publish', // if you don't want drafts to be returned
		'ignore_sticky_posts' => 1,
		'posts_per_page' => 50, // how much to show at once
		'post_type' => 'ordens_de_servico',
		'meta_query' => array(
	        'os_clause' => array(
	            'key' => 'os_pedido_criado',
	            'compare' => 'NOT EXISTS',
	        ), 
	    ),
	) );

	$terms = get_terms( array(
    	'taxonomy' => 'woo_os_problemas',
    	'hide_empty' => false,
	) );
	
	if( $search_results->have_posts() ) :
		while( $search_results->have_posts() ) : $search_results->the_post();	
			// shorten the title a little
			$title = ( mb_strlen( $search_results->post->post_title ) > 50 ) ? mb_substr( $search_results->post->post_title, 0, 49 ) . '...' : $search_results->post->post_title;
			$return[] = array( $search_results->post->ID, $title ); // array( Post ID, Post Title )
		endwhile;
	endif;
	echo json_encode( $terms );
	die;
}