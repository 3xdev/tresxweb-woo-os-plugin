<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<style type="text/css">
	body{
		margin-top: 0;
		font-size:  10px;
		text-transform: uppercase;
	}
	div{
			padding: 2px;
		}
	h3{
		margin-bottom: 0;
	}
	.os_header_loja{
		margin-top: -40px;
	}
	.divisoria{
		border-top: 2px dashed;
   		margin: 20px;
	}
	.os_header,
	.os_header_loja,
	.os_store_address,
	.os_id,
	.os_customer_data,
	.os_service_data_l,
	.os_service_data_c,
	.os_accept
	{    
	display: flex;
    justify-content: space-between;
    align-items: center;
    align-content: center;
	}
	.print_os{
		padding: 0 20px;
	}
	
	.os_consent{
		border: 1px solid #333;
	}
</style>
<body>
<div class="print_os">
<div class="os_header_loja">
	<div class="os_logo">
		<img src="/wp-content/uploads/woocommerce-placeholder.png" width="150px" height="150px">
	</div>
	<div class="os_store_address">
		<ul>
			<li><span class="os_nome_loja"></span></li>
			<li><span class="os_endereco"></span></li>
			<li><span class="os_complemento"></span></li>
			<li>Celular</li>
			<li><span class="os_site_url"></span></li>
			<li><span class="os_admin_email"></span></li>
		</ul>
	</div>
</div>
<div class="os_id">
	<div class="os_number">
		<p><b>OS Nº</b> <span class="os_number_p"></span></p>
		<p><b>Data:</b> <span class="os_data_p"></span></p>
	</div>
	<div class="os_info">
		<p>Acompanhe o andamento em www.thewatchclub.com.br</p>
		<p><b>Tipo:</b> <span class="os_tipo_p"></span></p>
	</div>
</div>

<h3>Cliente</h3>
<hr>
<div class="os_customer_data">
	<div>
		<p><b>Nome:</b> <span class="os_cliente_p"></span></p>
		<p><b>Endereço:</b> <span class="os_endereco_cliente_p"></span></p>
		<p><b>Telefone:</b> <span class="os_telefone_cliente_p"></span></p>
	</div>
	<div>
		<p><b>CPF/CNPJ:</b> <span class="os_cpf_cnpj_p"></span></p>
		<p><b>E-mail:</b> <span class="os_email_cliente_p"></span></p>
	</div>
</div>

<div class="service_container_c">
	<h3>Tipo</h3>
	<hr>
	<div class="os_service_data_c">
		<div>
			<p><b>Produto:</b> <span class="os_produto_p"></span></p>
			<p><b>Marca:</b> <span class="os_marca_p"></span></p>

			<p class="completa"><b>Mecanismo:</b> <span class="os_mecanismo_p">--</span></p>
			<p class="completa"><b>Calibre:</b> <span class="os_calibre_p">--</span></p>
		</div>
		<div>
			<p><b>Referência:</b> <span class="os_ref_p"></span></p>
			<p><b>Defeito(s) Declarados:</b> <span class="os_defeitos_p"></span></p>

			<p class="completa"><b>Validade: (dias)</b> <span class="os_validade_p">--</span></p>
			<p class="completa"><b>Tempo de Serviço: (dias)</b> <span class="os_tempo_p">--</span></p>
		</div>
		<div>
			<p class="completa"><b>Observações:</b> <span class="os_obs_p"></span></p>
			<p class="completa"><b>Características:</b> <span class="os_caracteristicas_p"></span></p>

			<p class="completa"><b>Previsão de entrega (dias):</b> <span class="os_previsao_p">--</span></p>
			<p class="completa"><b>Meses de garantia:</b> <span class="os_garantia_tempo_p">--</span></p>
		</div>
	</div>
</div>

<div class="os_consent">
	<p><span><b>Importante:</b></span></p>
	<p>
		Só entregamos o relógio mediante apresentação deste comprovante. Atendimento das 10h às 19h, de segunda a sexta, e das 10h às 18h aos sábados. Fechado aos domingos e feriados. Prazo de 5 dias úteis para orçamentos referentes a mecanismos e de 10 dias úteis para cotação de peças. Em caso do não comparecimento para retirada do relógio no prazo de 30 dias após a confirmação da conclusão do serviço, haverá cobrança de multa de 2% sobre o valor a ser faturado na entrega + 0,2% por dia de atraso por indenização dos serviços prestados.
	</p>
</div>

<div class="os_accept">
	<div class="accept_text">
		Estou de acordo com as informações citadas
	</div>
	<div class="signature">
		Assinatura: ___________________________
	</div>
</div>

</div>
<!-- Via cliente -->
<div class="divisoria"></div>
<div class="print_os">
<div class="os_header">
	<div class="os_logo">
		<img src="/wp-content/uploads/woocommerce-placeholder.png" width="150px" height="150px">
	</div>
	<div class="os_store_address">
		<ul>
			<li><span class="os_nome_loja"></span></li>
			<li><span class="os_endereco"></span></li>
			<li><span class="os_complemento"></span></li>
			<li>Celular</li>
			<li><span class="os_site_url"></span></li>
			<li><span class="os_admin_email"></span></li>
		</ul>
	</div>
</div>
<div class="os_id">
	<div class="os_number">
		<p><b>OS Nº</b> <span class="os_number_p"></span></p>
		<p><b>Data:</b> <span class="os_data_p"></span></p>
	</div>
	<div class="os_info">
		<p>Acompanhe o andamento em www.thewatchclub.com.br</p>
		<p><b>Tipo:</b> <span class="os_tipo_p"></span></p>
	</div>
</div>

<h3>Cliente</h3>
<hr>
<div class="os_customer_data">
	<div>
		<p><b>Nome:</b> <span class="os_cliente_p"></span></p>
		<p><b>Endereço:</b> <span class="os_endereco_cliente_p"></span></p>
		<p><b>Telefone:</b> <span class="os_telefone_cliente_p"></span></p>
	</div>
	<div>
		<p><b>CPF/CNPJ:</b> <span class="os_cpf_cnpj_p"></span></p>
		<p><b>E-mail:</b> <span class="os_email_cliente_p"></span></p>
	</div>
</div>
<div class="service_container_l">
	<h3>Tipo</h3>
	<hr>
	<div class="os_service_data_l">
		<div>
			<p><b>Produto:</b> <span class="os_produto_p"></span></p>
			<p><b>Marca:</b> <span class="os_marca_p"></span></p>

			<p class="completa"><b>Mecanismo:</b> <span class="os_mecanismo_p">--</span></p>
			<p class="completa"><b>Calibre:</b> <span class="os_calibre_p">--</span></p>
		</div>
		<div>
			<p><b>Referência:</b> <span class="os_ref_p"></span></p>
			<p><b>Defeito(s) Declarados:</b> <span class="os_defeitos_p"></span></p>

			<p class="completa"><b>Validade: (dias)</b> <span class="os_validade_p">--</span></p>
			<p class="completa"><b>Tempo de Serviço: (dias)</b> <span class="os_tempo_p">--</span></p>
		</div>
		<div>
			<p class="completa"><b>Observações:</b> <span class="os_obs_p"></span></p>
			<p class="completa"><b>Características:</b> <span class="os_caracteristicas_p"></span></p>

			<p class="completa"><b>Previsão de entrega (dias):</b> <span class="os_previsao_p">--</span></p>
			<p class="completa"><b>Meses de garantia:</b> <span class="os_garantia_tempo_p">--</span></p>
		</div>
	</div>
</div>

<!-- <div class="os_consent">
	<p><span><b>Importante:</b></span></p>
	<p>
		Só entregamos o relógio mediante apresentação deste comprovante. Atendimento das 10h às 19h, de segunda a sexta, e das 10h às 18h aos sábados. Fechado aos domingos e feriados. Prazo de 5 dias úteis para orçamentos referentes a mecanismos e de 10 dias úteis para cotação de peças. Em caso do não comparecimento para retirada do relógio no prazo de 30 dias após a confirmação da conclusão do serviço, haverá cobrança de multa de 2% sobre o valor a ser faturado na entrega + 0,2% por dia de atraso por indenização dos serviços prestados.
	</p>
</div> -->

<div class="os_accept">
	<div class="accept_text">
		Estou de acordo com as informações citadas
	</div>
	<div class="signature">
		Assinatura: ___________________________
	</div>
</div>

</div>
</body>
<script type="text/javascript">


	jQuery(function() {

		function checkVariable() {

		   if (typeof myVariable === 'undefined') {
		      
		    }else{
		    	var newText = JSON.parse(myVariable.responseText);
		    	// console.log(newText.woo_os_marca);
		    	// console.log('tamanho do array ' + newText['woo_os_marca'].length);
		    	// var value = newText['woo_os_marca'].length;
		    	var value = Object.keys(newText.woo_os_marca).length;

		    	if(newText.woo_os_tipo[0] == 1){
		    		jQuery( ".completa" ).hide();
		    		jQuery('.os_tipo_p').html("Expressa"); 
		    	}else{
		    		jQuery('.os_tipo_p').html("Completa"); 
		    	}
		    	// console.log(value);
		    	for (var i = 0; i <= value-1; i++) {
		    		// console.log('loop #'+i);

		    		jQuery('.os_service_data_c .os_marca_p').last().html(newText.woo_os_marca[i]); 
		    		jQuery('.os_service_data_c .os_produto_p').last().html(newText.woo_os_produto[i]); 
					jQuery('.os_service_data_c .os_defeitos_p').last().html(newText.woo_os_problemas[i]); 
					jQuery('.os_service_data_c .os_ref_p').last().html(newText.woo_os_referencia[i]);

					jQuery('.os_service_data_l .os_marca_p').last().html(newText.woo_os_marca[i]); 
		    		jQuery('.os_service_data_l .os_produto_p').last().html(newText.woo_os_produto[i]); 
					jQuery('.os_service_data_l .os_defeitos_p').last().html(newText.woo_os_problemas[i]); 
					jQuery('.os_service_data_l .os_ref_p').last().html(newText.woo_os_referencia[i]);

					if(newText.woo_os_tipo[0] == 2){

						jQuery('.os_service_data_c .os_obs_p').last().html(newText.woo_os_marca[i]); 
			    		jQuery('.os_service_data_c .os_caracteristicas_p').last().html(newText.woo_os_caracteristicas[i]); 
						jQuery('.os_service_data_l .os_obs_p').last().html(newText.woo_os_marca[i]); 
			    		jQuery('.os_service_data_l .os_caracteristicas_p').last().html(newText.woo_os_caracteristicas[i]);

			    		jQuery('.os_service_data_l .os_marca_p').last().html(newText.woo_os_marca[i]); 
			    		jQuery('.os_service_data_l .os_produto_p').last().html(newText.woo_os_produto[i]); 
						jQuery('.os_service_data_l .os_defeitos_p').last().html(newText.woo_os_problemas[i]); 
						jQuery('.os_service_data_l .os_ref_p').last().html(newText.woo_os_referencia[i]); 


			    		if(typeof newText.woo_os_mecanismo !== 'undefined'){
			    			jQuery('.os_service_data_c .os_mecanismo_p').last().html(newText.woo_os_mecanismo[i]); 
			    			jQuery('.os_service_data_l .os_mecanismo_p').last().html(newText.woo_os_mecanismo[i]);
			    		}		 
			    		if(typeof newText.woo_os_calibre !== 'undefined'){
			    			jQuery('.os_service_data_c .os_calibre_p').last().html(newText.woo_os_calibre[i]); 
			    			jQuery('.os_service_data_l .os_calibre_p').last().html(newText.woo_os_calibre[i]);
			    		}		 
						if(typeof newText.woo_os_validade !== 'undefined'){
							jQuery('.os_service_data_c .os_validade_p').last().html(newText.woo_os_validade[i]); 
							jQuery('.os_service_data_l .os_validade_p').last().html(newText.woo_os_validade[i]);
						}		 
						if(typeof newText.woo_os_tempo_servico !== 'undefined'){
							jQuery('.os_service_data_c .os_tempo_p').last().html(newText.woo_os_tempo_servico[i]);
							jQuery('.os_service_data_l .os_tempo_p').last().html(newText.woo_os_tempo_servico[i]);
						}		
						if(typeof newText.woo_os_previsao_entrega !== 'undefined'){
							jQuery('.os_service_data_c .os_previsao_p').last().html(newText.woo_os_previsao_entrega[i]); 
							jQuery('.os_service_data_l .os_previsao_p').last().html(newText.woo_os_previsao_entrega[i]);
						}		 
						if(typeof newText.woo_os_tempo_garantia !== 'undefined'){
							jQuery('.os_service_data_c .os_garantia_tempo_p').last().html(newText.woo_os_tempo_garantia[i]);
							jQuery('.os_service_data_l .os_garantia_tempo_p').last().html(newText.woo_os_tempo_garantia[i]);
						}		 
						
			    	}	

					if(i < value-1){
					  jQuery( ".os_service_data_c" ).first().clone().appendTo( ".service_container_c" );
					  jQuery( ".os_service_data_l" ).first().clone().appendTo( ".service_container_l" );
					  console.log('clone #'+i);
					}
					  
		    	}
		      	jQuery('.os_number_p').html(newText['woo_os_serial']);
				jQuery('.os_data_p').html(newText['data_os']);
				
				// preenche dados da loja
				jQuery('.os_nome_loja').html(newText['nome_loja']);
				jQuery('.os_endereco').html(newText['endereco']);
				jQuery('.os_complemento').html(newText['complemento']);
				jQuery('.os_site_url').html(newText['site_url']);
				jQuery('.os_admin_email').html(newText['admin_email']);
				// preenche dados do cliente
				jQuery('.os_cliente_p').html(newText['billing_first_name']);

				jQuery('.os_endereco_cliente_p').html(newText['billing_address_1']);
				jQuery('.os_endereco_cliente_p').append(', ', newText['billing_number']);
				jQuery('.os_endereco_cliente_p').append(', ', newText['billing_city']);
				jQuery('.os_endereco_cliente_p').append(', ', newText['billing_state']);
				jQuery('.os_endereco_cliente_p').append(', ', newText['billing_postcode']);

				jQuery('.os_telefone_cliente_p').html(newText['billing_phone']);
				jQuery('.os_cpf_cnpj_p').html(newText['documento']);
				jQuery('.os_email_cliente_p').html(newText['billing_email']);
				// preenche dados dos serviços
				// jQuery('.os_marca_p').html(newText['woo_os_marca']); 
				// jQuery('.os_caracteristicas_p').html(newText['woo_os_caracteristicas']); 
				// jQuery('.os_defeitos_p').html(newText['woo_os_problemas']); 
				jQuery('.os_obs_p').html(newText['woo_os_ext_obs']); 
				// jQuery('.os_ref_p').html(newText['woo_os_referencia']); 
		    }
	 	}
	 	setTimeout(checkVariable, 1000);


		
	});
</script>
</html>