<?php
defined( 'ABSPATH' ) || exit;
/** 
 * Update the order meta with field value
 */
add_action( 'woocommerce_order_details_after_order_table',
            'custom_field_display_cust_order_meta', 10, 1 );

function custom_field_display_cust_order_meta( $order ) {
  // echo '[whatever]';
  /* translators: whatever */
  if(get_post_meta( $order->get_id(), '_woo_os_serial', true )  ){
    $os_link =  get_post_meta( $order->get_id(), '_woo_os_importada', true );
    echo '<p>' . sprintf( __( '<strong>OS Vinculada:</strong> %1$s' ),
                        get_post_meta( $order->get_id(),'_woo_os_serial', true )  )
       . '<a class="button" href="/minha-conta/minha_os/?os='. $os_link .'" >Visualizar </a></p>';
  }
  
  // echo '[whatever]';
}

add_filter( 'woocommerce_account_orders_columns', 'add_account_orders_column', 10, 1 );
function add_account_orders_column( $columns ){
    $columns['custom-column'] = __( 'OS', 'woocommerce' );

    return $columns;
}

add_action( 'woocommerce_my_account_my_orders_column_custom-column', 'add_account_orders_column_rows' );
function add_account_orders_column_rows( $order ) {
    // Example with a custom field
    if ( $value = $order->get_meta( '_woo_os_serial' ) ) {
        echo esc_html( $value );
    }
}