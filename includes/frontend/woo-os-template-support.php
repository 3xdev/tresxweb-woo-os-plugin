<?php

// Na página de pagamento do pedido (checkout) trocar o título ou exibir um subtítulo abaixo de "Pagar pedido" da seguinte forma:
// Orçamento (marca do relógio) - OS XXX e data;

// Se não tiver OS, apenas "Orçamento (marca do relógio) e data.

// Caso a compra seja efetuada pelo site (e-commerce) exibir apenas "Pagar pedido" mesmo.


add_filter( 'the_title', 'change_pay_for_order_title' );
add_filter( 'the_content', 'change_pay_for_order_table', 1 );
 
// function filter_the_content_in_the_main_loop( $content ) {
 
//     // Check if we're inside the main loop in a single Post.
//     if ( is_singular() && in_the_loop() && is_main_query() ) {
//         return esc_html__( 'I’m filtering the content inside the main loop', 'wporg') . $content;
//     }
 
//     return $content;
// }
function change_pay_for_order_title( $title ) {

    if ( is_wc_endpoint_url( 'order-pay' ) ) {

        // pega o nr do pedido pela key passada no WC
        $order_number = wc_get_order_id_by_order_key( $_GET['key'] );

        // se houver uma OS vinculada
        if(get_post_meta($order_number, '_woo_os_serial', true)){
            // nr da os maqueado
            $nr_os_serial = get_post_meta($order_number, '_woo_os_serial', true);
            // nr da os original (id do post)        // 
            $nr_os_wp     = get_post_meta($order_number, '_woo_os_importada', true);
            // marca do relogio na OS
            $marca_orcamento = get_post_meta($nr_os_wp, 'woo_os_marca', true);
            // data em que a OS foi publicada
            $data_da_os = get_the_date('',$nr_os_wp);

            return __('Orçamento da OS '.$nr_os_serial, 'woocommerce');

        }elseif(!get_post_meta($order_number, '_woo_os_importada', true) && get_post_meta($order_number, '_woo_os_marcas', true)){
            
            // data em que a OS foi publicada
            $data_da_os = get_the_date('',$order_number);
            // marca do item vinculado ao pedido sem OS
            $marca_orcamento = get_post_meta($order_number, '_woo_os_marcas', true);

            return __('Orçamento '.$marca_orcamento, 'woocommerce');
        }
        
    }

    return $title;
}
function change_pay_for_order_table( $content ) {

    if ( is_wc_endpoint_url( 'order-pay' ) ) {

        $style ='<style type="text/css">.tabela-os{width: 100%; display: flex; flex-wrap: wrap; margin-bottom: 5%;}.tabela-os div{border: 1px solid #ccc; padding: 1% 1%; width: 100%; flex-basis: 100%;}.tabela-os .prazo, /*.tabela-os .garantia{flex-basis: 50%;}*/</style>';
        // pega o nr do pedido pela key passada no WC
        $order_number = wc_get_order_id_by_order_key( $_GET['key'] );

        // se houver uma OS vinculada
        if(get_post_meta($order_number, '_woo_os_serial', true)){
            // nr da os maqueado
            $nr_os_serial = get_post_meta($order_number, '_woo_os_serial', true);
            // nr da os original (id do post)        // 
            $nr_os_wp     = get_post_meta($order_number, '_woo_os_importada', true);
            // marca do relogio na OS
            $marca_orcamento = get_post_meta($nr_os_wp, 'woo_os_marca', true);
            // data em que a OS foi publicada
            $data_da_os = get_the_date('',$nr_os_wp);
            // validade do item vinculado ao pedido sem OS
            $validade_orcamento = get_post_meta($nr_os_wp, 'woo_os_validade', true);
            // previsao do item vinculado ao pedido sem OS
            $previsao_orcamento = get_post_meta($nr_os_wp, 'woo_os_previsao_entrega', true);
            // caracteristicas do item vinculado ao pedido sem OS
            $caracteristicas_orcamento = get_post_meta($nr_os_wp, 'woo_os_caracteristicas', true);
            // observações do item vinculado ao pedido sem OS
            $obs_orcamento = get_post_meta($nr_os_wp, 'woo_os_observacao_interna', true);
            // observações do item vinculado ao pedido sem OS
            $garantia_orcamento = get_post_meta($nr_os_wp, 'woo_os_garantia', true);

            // return __('Orçamento '.$marca_orcamento.' - OS '.$nr_os_serial.' Data '.$data_da_os, 'woocommerce') . $content;
            return __($style.'<div class="tabela-os">
                        <div class="marca"><b>Marca do relógio:</b> '.$marca_orcamento.'</div>
                        <div class="caracteristicas"><b>Características:</b> '.$caracteristicas_orcamento.'</div>
                        <div class="prazo"><b>Prazo de entrega: </b> '.$previsao_orcamento.' dias</div>
                        <div class="garantia"><b>Garantia:</b> '.$garantia_orcamento.' meses</div>
                        <div class="observacoes"><b>Observações:</b> '.$obs_orcamento.'</div>
                        <div class="validade"><b>Validade do orçamento:</b> '.$validade_orcamento.' dias</div>
                    </div>') . $content;

        }elseif(!get_post_meta($order_number, '_woo_os_importada', true) && get_post_meta($order_number, '_woo_os_marcas', true)){
            
            // data em que a OS foi publicada
            $data_da_os = get_the_date('',$order_number);
            // marca do item vinculado ao pedido sem OS
            $marca_orcamento = get_post_meta($order_number, '_woo_os_marcas', true);
            // validade do item vinculado ao pedido sem OS
            $validade_orcamento = get_post_meta($order_number, '_woo_os_validade', true);
            // previsao do item vinculado ao pedido sem OS
            $previsao_orcamento = get_post_meta($order_number, '_woo_os_previsao_entrega', true);
            // caracteristicas do item vinculado ao pedido sem OS
            $caracteristicas_orcamento = get_post_meta($order_number, '_woo_os_caracteristicas', true);
            // observações do item vinculado ao pedido sem OS
            $obs_orcamento = get_post_meta($order_number, '_woo_os_observacoes_da_os', true);



            return __($style.'<div class="tabela-os">
                        <div class="marca"><b>Marca do relógio:</b> '.$marca_orcamento.'</div>
                        <div class="caracteristicas"><b>Características:</b> '.$caracteristicas_orcamento.'</div>
                        <div class="prazo"><b>Prazo de entrega: </b> '.$previsao_orcamento.' dias</div>
                        <div class="observacoes"><b>Observações:</b> '.$obs_orcamento.'</div>
                        <div class="validade"><b>Validade do orçamento:</b> '.$validade_orcamento.' dias</div>
                    </div>') . $content;
        }
        
    }

    return $content;
}
function wpse309151_remove_title_filter_nav_menu( $nav_menu, $args ) {
    // we are working with menu, so remove the title filter
    remove_filter( 'the_title', 'change_pay_for_order_title', 10, 2 );
    return $nav_menu;
}
// this filter fires just before the nav menu item creation process
add_filter( 'pre_wp_nav_menu', 'wpse309151_remove_title_filter_nav_menu', 10, 2 );

function wpse309151_add_title_filter_non_menu( $items, $args ) {
    // we are done working with menu, so add the title filter back
    add_filter( 'the_title', 'change_pay_for_order_title', 10, 2 );
    return $items;
}
// this filter fires after nav menu item creation is done
add_filter( 'wp_nav_menu_items', 'wpse309151_add_title_filter_non_menu', 10, 2 );