<?php	
defined( 'ABSPATH' ) || exit;

class Minha_OS_My_Account_Endpoint {

	/**
	 * Custom endpoint name.
	 *
	 * @var string
	 */
	public static $endpoint = 'minha_os'; // HERE GOES THE ENDPOINT URL. IF YOU ADD courses THEN THE ENDPOINT URL WILL BE /my-account/courses/

	/**
	 * Plugin actions.
	 */
	public function __construct() {
		// Actions used to insert a new endpoint in the WordPress.
		add_action( 'init', array( $this, 'add_endpoints' ) );
		add_filter( 'query_vars', array( $this, 'add_query_vars' ), 0 );

		// Change the My Accout page title.
		add_filter( 'the_title', array( $this, 'endpoint_title' ) );

		// Insering your new tab/page into the My Account page.
		// add_filter( 'woocommerce_account_menu_items', array( $this, 'new_menu_items' ) );
		add_action( 'woocommerce_account_' . self::$endpoint .  '_endpoint', array( $this, 'endpoint_content' ) );
	}

	/**
	 * Register new endpoint to use inside My Account page.
	 *
	 * @see https://developer.wordpress.org/reference/functions/add_rewrite_endpoint/
	 */
	public function add_endpoints() {
		add_rewrite_endpoint( self::$endpoint, EP_ROOT | EP_PAGES );
	}

	/**
	 * Add new query var.
	 *
	 * @param array $vars
	 * @return array
	 */
	public function add_query_vars( $vars ) {
		$vars[] = self::$endpoint;

		return $vars;
	}

	/**
	 * Set endpoint title.
	 *
	 * @param string $title
	 * @return string
	 */
	public function endpoint_title( $title ) {
		global $wp_query;

		$is_endpoint = isset( $wp_query->query_vars[ self::$endpoint ] );

		if ( $is_endpoint && ! is_admin() && is_main_query() && in_the_loop() && is_account_page() ) {
			// New page title.
			$title = __( 'Minha OS', 'woocommerce' );

			remove_filter( 'the_title', array( $this, 'endpoint_title' ) );
		}

		return $title;
	}

	/**
	 * Insert the new endpoint into the My Account menu.
	 *
	 * @param array $items
	 * @return array
	 */
	// public function new_menu_items( $items ) {
	// 	// Remove the logout menu item.
	// 	$logout = $items['customer-logout'];
	// 	unset( $items['customer-logout'] );

	// 	// Insert your custom endpoint.
	// 	$items[ self::$endpoint ] = __( 'Minha OS', 'woocommerce' );

	// 	// Insert back the logout item.
	// 	$items['customer-logout'] = $logout;

	// 	return $items;
	// }

	/**
	 * Endpoint HTML content.
	 */
	public function endpoint_content() {

		$current_user = wp_get_current_user();
		
		$query = new WP_Query( array(
			  'p' => $_GET['os'],
		    'post_type' => 'ordens_de_servico',
		    'posts_per_page' => 30,
		    'meta_query' => array(
                        array(
                          'key' => 'woo_os_user',
                          'value' => $current_user->user_email,
                          'compare' => '='
                        )
                    )
		) );
		if ( $query->have_posts() ) {
		?>
		<style type="text/css">
			ul.order_notes li .note_content {
			    padding: 10px;
			    background: #efefef;
			    position: relative;
			}

			ul.order_notes {
			    padding: 2px 0 0;
			}
			.order_notes {
			  list-style: none;
			}
			ul.order_notes li .note_content::after {
			    content: "";
			    display: block;
			    position: absolute;
			    bottom: -10px;
			    left: 20px;
			    width: 0;
			    height: 0;
			    border-width: 10px 10px 0 0;
			    border-style: solid;
			    border-color: #efefef transparent;
			}
		</style>
		<table class="woocommerce-table woocommerce-table--order-details shop_table order_details">
				<thead>
					<tr>
						<th class="woocommerce-table__product-name product-name">Item</th>
						<th class="woocommerce-table__product-table product-total">Descrição</th>
					</tr>
				</thead>
				<tbody>
		<?php
		    while ( $query->have_posts() ) {
		        $query->the_post();
		        
		        // dados do produto
		        $tipo_de_os = get_post_meta(get_the_ID(), 'woo_os_tipo', true);
		        // 1 == expressa
		        // 2 == completa
    		  	$tipo = get_post_meta(get_the_ID(), 'woo_os_produto');
        		$marca = get_post_meta(get_the_ID(), 'woo_os_marca');
        		$referencia = get_post_meta(get_the_ID(), 'woo_os_referencia');
						$problemas = get_post_meta(get_the_ID(), 'woo_os_problemas');
						
						// dados do pedido
						$status = get_post_meta(get_the_ID(), 'woo_os_status', true);

							switch ($status) {
						    case "1":
						        $status = "Aguardando Orçamento";
						        break;
						    case "2":
						        $status = "Aguardando Pagamento";
						        break;
						    case "3":
						        $status = "Aguardando Material";
						        break;
						    case "4":
						        $status = "Orçamento Reprovado";
						        break;
						    case "5":
						        $status = "Em Execução";
						        break;
						    case "6":
						        $status = "Reprovado pelo Técnico";
						        break;
						    case "7":
						        $status = "Pronto";
						        break;
						    case "8":
						        $status = "Concluído";
						        break;
						    case "9":
						        $status = "Estornado";
						        break;
						    default:
						        $status = "Aguardando Orçamento";
						}

						$valor = get_post_meta(get_the_ID(), 'woo_os_valor', true);
						// $data = ;
						$validade = get_post_meta(get_the_ID(), 'woo_os_validade', true);
						$tempo = get_post_meta(get_the_ID(), 'woo_os_tempo_servico', true);
						$garantia = get_post_meta(get_the_ID(), 'woo_os_tempo_garantia', true);
						$orcamentista = get_post_meta(get_the_ID(), 'woo_os_orcamentista', true);	

				if ( isset($tipo) ) {
        foreach( $tipo as $index=>$value ) {
				?>
					<tr class="woocommerce-table__line-item order_item">
						<td class="woocommerce-table__product-name " style="font-weight: bold;">
							Produto 
							<?php echo ($tipo_de_os == 2) ? ( '' ): '#'.($index+1); ?>
						</td>
						<td class="woocommerce-table__product-total ">
							<?php echo $value; ?>
						</td>
					</tr>
					<tr class="woocommerce-table__line-item order_item">
						<td class="woocommerce-table__product-name " style="font-weight: bold;">
							Marca do produto 
							<?php echo ($tipo_de_os == 2) ? ( '' ): '#'.($index+1); ?>
						</td>
						<td class="woocommerce-table__product-total ">
							<?php echo $marca[$index]; ?>
						</td>
					</tr>
					<tr class="woocommerce-table__line-item order_item">
						<td class="woocommerce-table__product-name " style="font-weight: bold;">
							Referência
							<?php echo ($tipo_de_os == 2) ? ( '' ): '#'.($index+1); ?>
						</td>
						<td class="woocommerce-table__product-total ">
							<?php echo $referencia[$index]; ?>
						</td>
					</tr>
					<tr class="woocommerce-table__line-item order_item">
						<td class="woocommerce-table__product-name " style="font-weight: bold;">
							Problema
							<?php echo ($tipo_de_os == 2) ? ( '' ): '#'.($index+1); ?>
						</td>
						<td class="woocommerce-table__product-total ">
							<?php echo $problemas[$index]; ?>
						</td>
					</tr>
				<?php } 
			}?>
					<tr class="woocommerce-table__line-item order_item">
						<td class="woocommerce-table__product-name " style="font-weight: bold;">
							Status da OS
						</td>
						<td class="woocommerce-table__product-total ">
							<?php echo $status; ?>
						</td>
					</tr>
					<tr class="woocommerce-table__line-item order_item">
						<td class="woocommerce-table__product-name " style="font-weight: bold;">
							Valor do serviço
						</td>
						<td class="woocommerce-table__product-total ">
							R$ <?php echo $valor; ?>
						</td>
					</tr>
					<tr class="woocommerce-table__line-item order_item">
						<td class="woocommerce-table__product-name " style="font-weight: bold;">
							Data da OS
						</td>
						<td class="woocommerce-table__product-total ">
							<?php echo the_date(); ?>
						</td>
					</tr>
					<?php if($tipo_de_os == 2){?>
					<tr class="woocommerce-table__line-item order_item">
						<td class="woocommerce-table__product-name " style="font-weight: bold;">
							Validade da OS
						</td>
						<td class="woocommerce-table__product-total ">
							<?php echo $validade; ?> dias
						</td>
					</tr>
					<tr class="woocommerce-table__line-item order_item">
						<td class="woocommerce-table__product-name " style="font-weight: bold;">
							Tempo de Serviço
						</td>
						<td class="woocommerce-table__product-total ">
							<?php echo $tempo; ?> dias
						</td>
					</tr>
					<tr class="woocommerce-table__line-item order_item">
						<td class="woocommerce-table__product-name " style="font-weight: bold;">
							Garantia do serviço
						</td>
						<td class="woocommerce-table__product-total ">
							<?php echo $garantia; ?> meses
						</td>
					</tr>
					<!-- <tr class="woocommerce-table__line-item order_item">
						<td class="woocommerce-table__product-name " style="font-weight: bold;">
							Orçamentista
						</td>
						<td class="woocommerce-table__product-total ">
							<?php //echo $orcamentista; ?>
						</td>
					</tr> -->
				<?php }
		        
		    // Other things in the loop that you want to display
			// endwhile			
		    }
		    ?>
		    </tbody>
			</table>
			<?php
			WOO_OS_Box_Order_Notes::output(get_the_ID());
		}else{
			echo '<h2>Nenhuma ordem de serviço em seu nome.</h2>';
		}
	}

	/**
	 * Plugin install action.
	 * Flush rewrite rules to make our custom endpoint available.
	 */
	public static function install() {
		flush_rewrite_rules();
	}
}

new Minha_OS_My_Account_Endpoint();

// Flush rewrite rules on plugin activation.
register_activation_hook( __FILE__, array( 'Minha OS', 'install' ) );