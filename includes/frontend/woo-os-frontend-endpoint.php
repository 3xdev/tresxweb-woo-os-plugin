<?php	
defined( 'ABSPATH' ) || exit;

class Minhas_OS_My_Account_Endpoint {

	/**
	 * Custom endpoint name.
	 *
	 * @var string
	 */
	public static $endpoint = 'minhas_os'; // HERE GOES THE ENDPOINT URL. IF YOU ADD courses THEN THE ENDPOINT URL WILL BE /my-account/courses/

	/**
	 * Plugin actions.
	 */
	public function __construct() {
		// Actions used to insert a new endpoint in the WordPress.
		add_action( 'init', array( $this, 'add_endpoints' ) );
		add_filter( 'query_vars', array( $this, 'add_query_vars' ), 0 );

		// Change the My Accout page title.
		add_filter( 'the_title', array( $this, 'endpoint_title' ) );

		// Insering your new tab/page into the My Account page.
		add_filter( 'woocommerce_account_menu_items', array( $this, 'new_menu_items' ) );
		add_action( 'woocommerce_account_' . self::$endpoint .  '_endpoint', array( $this, 'endpoint_content' ) );
	}

	/**
	 * Register new endpoint to use inside My Account page.
	 *
	 * @see https://developer.wordpress.org/reference/functions/add_rewrite_endpoint/
	 */
	public function add_endpoints() {
		add_rewrite_endpoint( self::$endpoint, EP_ROOT | EP_PAGES );
	}

	/**
	 * Add new query var.
	 *
	 * @param array $vars
	 * @return array
	 */
	public function add_query_vars( $vars ) {
		$vars[] = self::$endpoint;

		return $vars;
	}

	/**
	 * Set endpoint title.
	 *
	 * @param string $title
	 * @return string
	 */
	public function endpoint_title( $title ) {
		global $wp_query;

		$is_endpoint = isset( $wp_query->query_vars[ self::$endpoint ] );

		if ( $is_endpoint && ! is_admin() && is_main_query() && in_the_loop() && is_account_page() ) {
			// New page title.
			$title = __( 'Minhas OS', 'woocommerce' );

			remove_filter( 'the_title', array( $this, 'endpoint_title' ) );
		}

		return $title;
	}

	/**
	 * Insert the new endpoint into the My Account menu.
	 *
	 * @param array $items
	 * @return array
	 */
	public function new_menu_items( $items ) {
		// Remove the logout menu item.
		$logout = $items['customer-logout'];
		unset( $items['customer-logout'] );

		// Insert your custom endpoint.
		$items[ self::$endpoint ] = __( 'Minhas OS', 'woocommerce' );

		// Insert back the logout item.
		$items['customer-logout'] = $logout;

		return $items;
	}

	/**
	 * Endpoint HTML content.
	 */
	public function endpoint_content() {

		$current_user = wp_get_current_user();
		
		$query = new WP_Query( array(
		    'post_type' => 'ordens_de_servico',
		    'posts_per_page' => 30,
		    'meta_query' => array(
                        array(
                          'key' => 'woo_os_user',
                          'value' => $current_user->user_email,
                          'compare' => '='
                        )
                    )
		) );
		if ( $query->have_posts() ) {
		?>
		<style type="text/css">
			.order_buttons_os .cancel,
			.order_buttons_os .view{
			    display: none!important;
			}
		</style>
		<table class="woocommerce-orders-table shop_table shop_table_responsive my_account_orders account-orders-table">
		<thead>
			<tr>
				<th class="woocommerce-orders-table__header"><span class="nobr">OS</span></th>
				<th class="woocommerce-orders-table__header"><span class="nobr">Data</span></th>
				<th class="woocommerce-orders-table__header"><span class="nobr">Marca</span></th>
				<th class="woocommerce-orders-table__header"><span class="nobr">Status</span></th>
				<th class="woocommerce-orders-table__header"><span class="nobr">Valor</span></th>
				<th class="woocommerce-orders-table__header"><span class="nobr">Ações</span></th>
		</tr>
		</thead>
		<tbody>
		<?php
		    while ( $query->have_posts() ) {
		        $query->the_post();
		        
		        // $produto = get_post_meta(get_the_ID(), 'woo_os_produto', true);
		        $tipo = get_post_meta(get_the_ID(), 'woo_os_produto', true);
		        $marca = get_post_meta(get_the_ID(), 'woo_os_marca', true);
				// $problemas = get_post_meta(get_the_ID(), 'woo_os_problemas', true);
				
				$referencia = get_post_meta(get_the_ID(), 'woo_os_referencia', true);
				$status = get_post_meta(get_the_ID(), 'woo_os_status', true);
				// "1" = "Aguardando Orçamento";
				// "2" = "Aguardando Pagamento";
				// "3" = "Aguardando Material";
				// "4" = "Orçamento Reprovado";
				// "5" = "Em Execução";
				// "6" = "Reprovado pelo Técnico";
				// "7" = "Pronto";
				// "8" = "Concluído";
				// "9" = "Estornado";
				switch ($status) {
					    case "1":
					        $status = "Aguardando Orçamento";
					        break;
					    case "2":
					        $status = "Aguardando Pagamento";
					        break;
					    case "3":
					        $status = "Aguardando Material";
					        break;
					    case "4":
					        $status = "Orçamento Reprovado";
					        break;
					    case "5":
					        $status = "Em Execução";
					        break;
					    case "6":
					        $status = "Reprovado pelo Técnico";
					        break;
					    case "7":
					        $status = "Pronto";
					        break;
					    case "8":
					        $status = "Concluído";
					        break;
					    case "9":
					        $status = "Estornado";
					        break;
					    default:
					        $status = "Aguardando Orçamento";
					}

				$valor = get_post_meta(get_the_ID(), 'woo_os_valor', true);
				$pedido = get_post_meta(get_the_ID(), 'os_pedido_criado', true);
				?>

			<tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-pending order">
				<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number" data-title="OS">
					<!-- #<?php //the_title(); ?> -->
					<?php the_title( sprintf( '<a href="%s" >#', esc_url( '/minha-conta/minha_os/?os='.get_the_ID() ) ), '</a>' ); ?>
				</td>
				<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Data">
					<time ><?php echo get_the_date(''); ?></time>
				</td>
				<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status" data-title="Marca">
					<?php echo $marca; ?>
				</td>
				<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status" data-title="Status">
					<?php echo $status; ?>
				</td>
				<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-total" data-title="Valor">
					<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">R$</span><?php echo $valor; ?></span>
				</td>
				<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions order_buttons_os" data-title="Ações">
					<?php
					 $order = wc_get_order( $pedido );
					 if($order){
						 	$actions = wc_get_account_orders_actions( $order );
						 	echo '<a class="button" href="'. esc_url( '/minha-conta/minha_os/?os='.get_the_ID() ) .'" >Visualizar </a>' ;
							if ( ! empty( $actions ) ) {
								foreach ( $actions as $key => $action ) { 
								// print_r( $action );
								// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited

									echo '<a href="' . esc_url( $action['url'] ) . '" class="button ' . sanitize_html_class( $key ) . '">' . esc_html( $action['name'] ) . '</a>';
								}
							} 
					 }else{
					 	echo 'Nenhum pedido vinculado';
					 }
					?>
			</tr>

				<?php
		     
		    // Other things in the loop that you want to display
			// endwhile			
		    }
		    ?>
		    </tbody>
			</table>
			<?php
		}else{
			echo '<h2>Nenhuma ordem de serviço em seu nome.</h2>';
		}
	}

	/**
	 * Plugin install action.
	 * Flush rewrite rules to make our custom endpoint available.
	 */
	public static function install() {
		flush_rewrite_rules();
	}
}

new Minhas_OS_My_Account_Endpoint();

// Flush rewrite rules on plugin activation.
register_activation_hook( __FILE__, array( 'Minhas OS', 'install' ) );