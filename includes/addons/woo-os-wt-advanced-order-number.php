<?php
if (!defined('WPINC')) {
    die;
}

if ( class_exists( 'Wt_Advanced_Order_Number' ) ) {
    add_option( 'wt-order-number', 'on', '', 'yes' );

}else{
    if(option_exists( 'wt-order-number' )){
            update_option( 'wt-order-number', 'off', 'yes' );
    }
}
