<?php

function woo_os_woocommerce_update_order( $order_get_id ) { 
    // make action magic happen here... 
     $order = new WC_Order( $order_get_id );
     $id_da_os = $order->get_meta('_woo_os_importada');

     $total_servicos = $order->get_total();

     update_post_meta($id_da_os, 'woo_os_valor', $total_servicos);
}; 
         
// add the action 
add_action( 'woocommerce_update_order', 'woo_os_woocommerce_update_order', 10, 1 ); 


add_action( 'save_post', 'woo_os_status_change_custom', 11, 3 );
function woo_os_status_change_custom($post_id, $post, $update) {

    // "1" = "Aguardando Orçamento";
    // "2" = "Aguardando Pagamento";
    // "3" = "Aguardando Material";
    // "4" = "Orçamento Reprovado";
    // "5" = "Em Execução";
    // "6" = "Reprovado pelo Técnico";
    // "7" = "Pronto";
    // "8" = "Concluído";
    // "9" = "Estornado";

    if(get_post_type($post_id) === 'ordens_de_servico'){

        $status_da_os = get_post_meta($post_id, 'woo_os_status', true);

        if(isset($status_da_os) ){
        // $status = get_post_meta(get_the_ID(), 'woo_os_status', true);
            switch ($status_da_os) {
                case 1:
                    $os_status_description = "Aguardando Orçamento";
                    break;
                case 2:
                    $os_status_description = "Aguardando Pagamento";
                    break;
                case 3:
                    $os_status_description = "Aguardando Material";
                    break;
                case 4:
                    $os_status_description = "Orçamento Reprovado";
                    break;
                case 5:
                    $os_status_description = "Em Execução";
                    break;
                case 6:
                    $os_status_description = "Reprovado pelo Técnico";
                    break;
                case 7:
                    $os_status_description = "Pronto";
                    break;
                case 8:
                    $os_status_description = "Concluído";
                    break;
                case 9:
                    $os_status_description = "Estornado";
                    break;
                default: 
                    $os_status_description = "Os Criada";                  
            }   
            // update_post_meta($post_id, 'woo_os_status', $os_status);

            $current_user = wp_get_current_user();
            $commentdata['comment_post_ID'] = $post_id;
            $commentdata['comment_author'] = $current_user->user_firstname;
            $commentdata['comment_author_email'] = $current_user->user_email;
            $commentdata['comment_content'] = "Status da os alterado para " . $os_status_description;
            $commentdata['comment_type'] = "order_note";

            
            wp_insert_comment( $commentdata );
        }

    }
    
}