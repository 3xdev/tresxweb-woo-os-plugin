<?php
defined( 'ABSPATH' ) || exit;
// Adding Meta container admin shop_order pages
add_action( 'add_meta_boxes', 'woo_os_order_add_meta_boxes' );
if ( ! function_exists( 'woo_os_order_add_meta_boxes' ) )
{
    function woo_os_order_add_meta_boxes()
    {
        add_meta_box( 'mv_other_fields', __('OS vinculada','woocommerce'), 'mv_add_other_fields_for_packaging', 'shop_order', 'side', 'core' );
        add_meta_box( 'woo_os_marca', __('Dados do Orçamento','woocommerce'), 'mv_add_other_fields_for_woo_os_marca', 'shop_order', 'side', 'core' );
    }
}


// Adding Meta field in the meta container admin shop_order pages
if ( ! function_exists( 'mv_add_other_fields_for_packaging' ) )
{
    function mv_add_other_fields_for_packaging()
    {
        global $post;

        $meta_field_data = get_post_meta( $post->ID, '_woo_os_importada', true ) ? get_post_meta( $post->ID, '_woo_os_importada', true ) : '';

        $serial_field_data = get_post_meta( $post->ID, '_woo_os_serial', true ) ? get_post_meta( $post->ID, '_woo_os_serial', true ) : '';

        echo '<input type="hidden" name="mv_other_meta_field_nonce" value="' . wp_create_nonce() . '">
            <p>
            
            <input type="hidden" style="width:250px;" name="os_do_pedido" id="os_do_pedido" placeholder="' . $meta_field_data . '" value="' . $meta_field_data . '" readonly="true">
            <input type="hidden" style="width:250px;" name="serial_do_pedido" placeholder="' . $serial_field_data . '" value="' . $serial_field_data . '" readonly="true">
            </p>';

        // NOVO SELECT DE OS
        if(!empty($serial_field_data)){
            echo '<select class="woo_os_search" id="woo_os_search" name="woo_os_search" style="width:100%" disabled><option value="'.$serial_field_data.'" selected="selected">'.$serial_field_data.'</option></select>';
        }else{
            echo '<select class="woo_os_search" id="woo_os_search" name="woo_os_search" style="width:100%"></select>';
        }

        if(!empty($serial_field_data)){
            echo '<br><br><input class="button-secondary" type="button" name="ver_os" id="ver_os" value="Ver OS" />';
        }


    }
}

// Adding Meta field in the meta container admin shop_order pages
if ( ! function_exists( 'mv_add_other_fields_for_woo_os_marca' ) )
{
    function mv_add_other_fields_for_woo_os_marca()
    {
        global $post;

        $woo_os_marca_field_data = get_post_meta( $post->ID, '_woo_os_importada', true ) ? get_post_meta( $post->ID, '_woo_os_importada', true ) : NULL;

        if(get_post_meta( $post->ID, '_woo_os_importada', true )){
            $marca_da_os = get_post_meta($woo_os_marca_field_data, 'woo_os_marca', true);
            $validade_da_os = get_post_meta($woo_os_marca_field_data, 'woo_os_validade', true);
            $prazo_da_os = get_post_meta($woo_os_marca_field_data, 'woo_os_previsao_entrega', true);
            $caracteristicas_da_os = get_post_meta($woo_os_marca_field_data, 'woo_os_caracteristicas', true);
            $observacoes_da_os = get_post_meta($woo_os_marca_field_data, 'woo_os_observacao_interna', true);
            $disabled = 'disabled';
        }else{
            $marca_da_os = get_post_meta($post->ID, '_woo_os_marcas', true );
            $validade_da_os = get_post_meta($post->ID, '_woo_os_validade', true);
            $prazo_da_os = get_post_meta($post->ID, '_woo_os_previsao_entrega', true);
            $caracteristicas_da_os = get_post_meta($post->ID, '_woo_os_caracteristicas', true);
            $observacoes_da_os = get_post_meta($post->ID, '_woo_os_observacoes_da_os', true);
            $disabled = '';
        }
                            
        echo '<p style="border-bottom:solid 1px #eee;padding-bottom:13px;">
            <label for="marca_da_os">Marca</label>
            <input type="text" style="width:250px;" name="marca_da_os" placeholder="' . $marca_da_os . '" value="' . $marca_da_os . '" '.$disabled.'>
            <label for="validade_da_os">Validade</label>
            <input type="text" style="width:250px;" name="validade_da_os" placeholder="' . $validade_da_os . '" value="' . $validade_da_os . '" '.$disabled.'>
            <label for="prazo_da_os">Prazo</label>
            <input type="text" style="width:250px;" name="prazo_da_os" placeholder="' . $prazo_da_os . '" value="' . $prazo_da_os . '" '.$disabled.'>
            <label for="caracteristicas_da_os">Características</label>
            <input type="text" style="width:250px;" name="caracteristicas_da_os" placeholder="' . $caracteristicas_da_os . '" value="' . $caracteristicas_da_os . '" '.$disabled.'>
            <label for="observacoes_da_os">Observações</label>
            <input type="text" style="width:250px;" name="observacoes_da_os" placeholder="' . $observacoes_da_os . '" value="' . $observacoes_da_os . '" '.$disabled.'>
            </p>';
    }
}

// Save the data of the Meta field
add_action( 'save_post', 'mv_save_wc_order_other_fields', 10, 1 );
if ( ! function_exists( 'mv_save_wc_order_other_fields' ) )
{

    function mv_save_wc_order_other_fields( $post_id ) {

        // We need to verify this with the proper authorization (security stuff).

        // Check if our nonce is set.
        if ( ! isset( $_POST[ 'mv_other_meta_field_nonce' ] ) ) {
            return $post_id;
        }
        $nonce = $_REQUEST[ 'mv_other_meta_field_nonce' ];

        //Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $nonce ) ) {
            return $post_id;
        }

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return $post_id;
        }

        // Check the user's permissions.
        if ( 'page' == $_POST[ 'post_type' ] ) {

            if ( ! current_user_can( 'edit_page', $post_id ) ) {
                return $post_id;
            }
        } else {

            if ( ! current_user_can( 'edit_post', $post_id ) ) {
                return $post_id;
            }
        }
        // --- Its safe for us to save the data ! --- //

        // Sanitize user input  and update the meta field in the database.
        if(isset($_POST[ 'woo_os_search' ])){
            update_post_meta( $post_id, '_woo_os_importada', $_POST[ 'woo_os_search' ] );
            update_post_meta( $post_id, '_woo_os_serial', get_the_title($_POST[ 'woo_os_search' ]) );
            update_post_meta( $_POST[ 'woo_os_search' ], 'os_pedido_criado', $post_id );
        }

        update_post_meta( $post_id, '_woo_os_marcas', $_POST[ 'marca_da_os' ] );
        // Sanitize user input  and update the meta field in the database.
        update_post_meta( $post_id, '_woo_os_validade', $_POST[ 'validade_da_os' ] );
        update_post_meta( $post_id, '_woo_os_previsao_entrega', $_POST[ 'prazo_da_os' ] );
        update_post_meta( $post_id, '_woo_os_caracteristicas', $_POST[ 'caracteristicas_da_os' ] );
        update_post_meta( $post_id, '_woo_os_observacoes_da_os', $_POST[ 'observacoes_da_os' ] );
    }
}