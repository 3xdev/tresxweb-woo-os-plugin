<?php
defined( 'ABSPATH' ) || exit;
// "1" = "Aguardando Orçamento";
// "2" = "Aguardando Pagamento";
// "3" = "Aguardando Material";
// "4" = "Orçamento Reprovado";
// "5" = "Em Execução";
// "6" = "Reprovado pelo Técnico";
// "7" = "Pronto";
// "8" = "Concluído";
// "9" = "Estornado";

add_action('woocommerce_order_status_changed','woo_order_status_change_custom', 10 , 3);

function woo_order_status_change_custom($order_id,$old_status,$new_status) {

    $order = new WC_Order( $order_id );
    $id_da_os = $order->get_meta('_woo_os_importada');

    // $orderstatus = $order->status;

    if( isset($id_da_os) ){       
        // $status = get_post_meta(get_the_ID(), 'woo_os_status', true);
        switch ($new_status) {
            case "pending":
                $os_status = 2;
                $os_status_description = "Aguardando Pagamento";
                break;
            case "on-hold":
                $os_status = 3;
                $os_status_description = "Aguardando Material";
                break;
            case "failed":
                $os_status = 2;
                $os_status_description = "Aguardando Pagamento";
                break;
            case "cancelled":
                $os_status = 4;
                $os_status_description = "Orçamento Reprovado";
                break;
            case "processing":
                $os_status = 5;
                $os_status_description = "Em Execução";
                break;
            case "completed":
                $os_status = 7;
                $os_status_description = "Pronto";
                break;
            case "refunded":
                $os_status = 6;
                $os_status_description = "Reprovado pelo Técnico";
                break;
            default:
                $os_status = 1;
                $os_status_description = "Aguardando Orçamento";
        }
        update_post_meta($id_da_os, 'woo_os_status', $os_status);

        $current_user = wp_get_current_user();
        $commentdata['comment_post_ID'] = $id_da_os;
        $commentdata['comment_author'] = $current_user->user_firstname;
        $commentdata['comment_author_email'] = $current_user->user_email;
        $commentdata['comment_content'] = "Pedido alterou o status da OS para " . $os_status_description;
        $commentdata['comment_type'] = "order_note";

        
        wp_insert_comment( $commentdata );
    }
}