<?php
/**
 * Order Notes
 *
 * @package WooCommerce\Admin\Meta Boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WC_Meta_Box_Order_Notes Class.
 */
class WOO_OS_Box_Order_Notes {

	/**
	 * Output the metabox.
	 *
	 * @param WP_Post $post Post object.
	 */
	public static function output( $post ) {
		global $post;

		$args = array(
			// 'order_id' => $post->ID,
			'order_id' => $post->ID,
		);

		$notes = wc_get_order_notes( $args );

		include __DIR__ . '/views/html-os-notas.php';
		?>
		<?php
	}
}
