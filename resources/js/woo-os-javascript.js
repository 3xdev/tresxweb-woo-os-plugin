/**
* Initialize frontend actions
*/
 function mask_woo_init() {
    
        person_type_fields();
        maskGeneral();
        os_type_fields();
        os_disable_edit();
    
    if (jQuery('#billing_country').val() === 'BR' ) {
        maskBilling();
    }
}
function os_disable_edit()  {
    if(jQuery('#woo_os_status').val() >= 8){
      jQuery("#woo_os_client :input, #woo_os_data :input, #authordiv :input, #woo_os_order_data :input, .complementos-pj :input, .complementos-pf :input")
        .prop("disabled", true);
      jQuery('.dashicons-insert').hide();
    }
}

function person_type_fields()  {
            // console.log('tipo de pessoa alterado');
    jQuery( '#billing_persontype' ).on( 'change', function () {
        var current = jQuery( this ).val();
        console.log('ocultar pfpj');
        // campos pf
        jQuery( '.complementos-pj ' ).hide();
        jQuery( '#billing_cpf ' ).hide().prop( "disabled", true );
        jQuery("label[for='billing_cpf']").hide().prop( "disabled", true );
        jQuery( '#billing_rg' ).hide().prop( "disabled", true );
        jQuery("label[for='billing_rg']").hide().prop( "disabled", true );
        // campos pj
        jQuery( '#billing_company' ).hide().prop( "disabled", true );
        jQuery("label[for='billing_company']").hide().prop( "disabled", true );
        jQuery( '#billing_cnpj' ).hide().prop( "disabled", true );
        jQuery("label[for='billing_cnpj']").hide().prop( "disabled", true );
        jQuery( '#billing_ie' ).hide().prop( "disabled", true );
        jQuery("label[for='billing_ie']").hide().prop( "disabled", true );

        if ( '1' === current ) {
            console.log('mostrar pf');
            jQuery( '#billing_cpf' ).show().prop( "disabled", false );
            jQuery("label[for='billing_cpf']").show().prop( "disabled", false );
            jQuery( '#billing_rg' ).show().prop( "disabled", false );
            jQuery("label[for='billing_rg']").show().prop( "disabled", false );
        }

        if ( '2' === current ) {
            console.log('mostrar pj');
            jQuery( '.complementos-pj ' ).show();
            jQuery( '#billing_company' ).show().prop( "disabled", false );
            jQuery("label[for='billing_company']").show().prop( "disabled", false );
            jQuery( '#billing_cnpj' ).show().prop( "disabled", false );
            jQuery("label[for='billing_cnpj']").show().prop( "disabled", false );
            jQuery( '#billing_ie' ).show().prop( "disabled", false );
            jQuery("label[for='billing_ie']").show().prop( "disabled", false );
        }
        os_disable_edit();
    }).change();
    
}

function os_type_fields() {
    // console.log('tipo de os alterado');
    jQuery( '#woo_os_tipo' ).on( 'change', function () {
        var current_os_tipo = jQuery( this ).val();
        jQuery('.dashicons-insert').hide();
        jQuery( '.complementos-os').hide();
        jQuery( '#woo_os_orcamento').hide();
        jQuery( '#woo_os_caracteristicas').hide().prop("disabled", true);
        // jQuery( '#woo_os_problemas').hide().prop("disabled", true);
        jQuery( '#woo_os_ext_obs').hide().prop("disabled", true);
        jQuery( '#woo_os_atendente').hide().prop("disabled", true);
        jQuery( '#woo_os_mecanismo').hide().prop("disabled", true);
        jQuery( '#woo_os_calibre').hide().prop("disabled", true);
        jQuery( '#woo_os_validade').hide().prop("disabled", true);
        jQuery( '#woo_os_tempo_servico').hide().prop("disabled", true);
        jQuery( '#woo_os_previsao_entrega').hide().prop("disabled", true);
        jQuery( '#woo_os_tempo_garantia').hide().prop("disabled", true);
        jQuery( '#woo_os_observacao_interna').hide().prop("disabled", true);
        jQuery( '#woo_os_orcamentista').hide().prop("disabled", true);

        jQuery( "label[for='woo_os_caracteristicas']").hide();
        // jQuery( "label[for='woo_os_problemas']").hide();
        jQuery( "label[for='woo_os_ext_obs']").hide();
        jQuery( "label[for='woo_os_atendente']").hide();
        jQuery( "label[for='woo_os_mecanismo']").hide();
        jQuery( "label[for='woo_os_calibre']").hide();
        jQuery( "label[for='woo_os_validade']").hide();
        jQuery( "label[for='woo_os_tempo_servico']").hide();
        jQuery( "label[for='woo_os_previsao_entrega']").hide();
        jQuery( "label[for='woo_os_tempo_garantia']").hide();
        jQuery( "label[for='woo_os_observacao_interna']").hide();
        jQuery( "label[for='woo_os_orcamentista']").hide();

        if ( '1' === current_os_tipo ) {
            jQuery( '.complementos-os').hide();
            jQuery('.dashicons-insert').show();
            jQuery( '#woo_os_orcamento').hide();
            jQuery( '#woo_os_caracteristicas').hide().prop("disabled", true);
            // jQuery( '#woo_os_problemas').hide().prop("disabled", true);
            jQuery( '#woo_os_ext_obs').hide().prop("disabled", true);
            jQuery( '#woo_os_atendente').hide().prop("disabled", true);
            jQuery( '#woo_os_mecanismo').hide().prop("disabled", true);
            jQuery( '#woo_os_calibre').hide().prop("disabled", true);
            jQuery( '#woo_os_validade').hide().prop("disabled", true);
            jQuery( '#woo_os_tempo_servico').hide().prop("disabled", true);
            jQuery( '#woo_os_previsao_entrega').hide().prop("disabled", true);
            jQuery( '#woo_os_tempo_garantia').hide().prop("disabled", true);
            jQuery( '#woo_os_observacao_interna').hide().prop("disabled", true);
            jQuery( '#woo_os_orcamentista').hide().prop("disabled", true);

            jQuery( "label[for='woo_os_caracteristicas']").hide();
            // jQuery( "label[for='woo_os_problemas']").hide();
            jQuery( "label[for='woo_os_ext_obs']").hide();
            jQuery( "label[for='woo_os_atendente']").hide();
            jQuery( "label[for='woo_os_mecanismo']").hide();
            jQuery( "label[for='woo_os_calibre']").hide();
            jQuery( "label[for='woo_os_validade']").hide();
            jQuery( "label[for='woo_os_tempo_servico']").hide();
            jQuery( "label[for='woo_os_previsao_entrega']").hide();
            jQuery( "label[for='woo_os_tempo_garantia']").hide();
            jQuery( "label[for='woo_os_observacao_interna']").hide();
            jQuery( "label[for='woo_os_orcamentista']").hide();
        }

        if ( '2' === current_os_tipo ) {
            jQuery( '.complementos-os').show();
            jQuery('.dashicons-insert').hide();
            jQuery( '#woo_os_orcamento').show();
            jQuery( '#woo_os_caracteristicas').show().prop("disabled", false);
            // jQuery( '#woo_os_problemas').show().prop("disabled", false);
            jQuery( '#woo_os_ext_obs').show().prop("disabled", false);
            jQuery( '#woo_os_atendente').show().prop("disabled", false);
            jQuery( '#woo_os_mecanismo').show().prop("disabled", false);
            jQuery( '#woo_os_calibre').show().prop("disabled", false);
            jQuery( '#woo_os_validade').show().prop("disabled", false);
            jQuery( '#woo_os_tempo_servico').show().prop("disabled", false);
            jQuery( '#woo_os_previsao_entrega').show().prop("disabled", false);
            jQuery( '#woo_os_tempo_garantia').show().prop("disabled", false);
            jQuery( '#woo_os_observacao_interna').show().prop("disabled", false);
            jQuery( '#woo_os_orcamentista').show().prop("disabled", false);

            jQuery( "label[for='woo_os_caracteristicas']").show();
            // jQuery( "label[for='woo_os_problemas']").show();
            jQuery( "label[for='woo_os_ext_obs']").show();
            jQuery( "label[for='woo_os_atendente']").show();
            jQuery( "label[for='woo_os_mecanismo']").show();
            jQuery( "label[for='woo_os_calibre']").show();
            jQuery( "label[for='woo_os_validade']").show();
            jQuery( "label[for='woo_os_tempo_servico']").show();
            jQuery( "label[for='woo_os_previsao_entrega']").show();
            jQuery( "label[for='woo_os_tempo_garantia']").show();
            jQuery( "label[for='woo_os_observacao_interna']").show();
            jQuery( "label[for='woo_os_orcamentista']").show();
        }
    }).change();
    
}

function maskBilling() {
    maskPhone( '#billing_phone, #billing_cellphone' );
    jQuery( '#billing_birthdate' ).mask( '00/00/0000' );
    jQuery( '#billing_postcode' ).mask( '00000-000' );
    jQuery( '#billing_phone, #billing_cellphone, #billing_birthdate, #billing_postcode' ).attr( 'type', 'tel' );
}

function maskGeneral() {
    jQuery( '#billing_cpf, #credit-card-cpf' ).mask( '000.000.000-00' );
    jQuery( '#billing_cnpj' ).mask( '00.000.000/0000-00' );
    maskPhone( '#credit-card-phone' );
}

function maskPhone(selector) {
    var jQueryelement = jQuery(selector),
            MaskBehavior = function(val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            maskOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(MaskBehavior.apply({}, arguments), options);
                }
            };

    jQueryelement.mask(MaskBehavior, maskOptions);
}

    
// jQuery( '#billing_persontype' ).on( 'change', person_type_fields() );
// jQuery( '#woo_os_tipo' ).on( 'change', os_type_fields() );

// wc_ecfb_frontend.init();

function limpa_formulário_cep() {
            //Limpa valores do formulário de cep.
        document.getElementById('billing_address_1').value=("");
        document.getElementById('billing_neighborhood').value=("");
        document.getElementById('billing_city').value=("");
        document.getElementById('billing_state').value=("");
        // document.getElementById('ibge').value=("");
}

function meu_callback(conteudo) {
    if (!("erro" in conteudo)) {
        //Atualiza os campos com os valores.
        document.getElementById('billing_address_1').value=(conteudo.logradouro);
        document.getElementById('billing_neighborhood').value=(conteudo.bairro);
        document.getElementById('billing_city').value=(conteudo.localidade);
        document.getElementById('billing_state').value=(conteudo.uf);
        // document.getElementById('ibge').value=(conteudo.ibge);

        // var billing_state = document.getElementById("dummy_billing_state");
        // document.getElementById("billing_state").value = billing_state.options[billing_state.selectedIndex].text;
        
    } //end if.
    else {
        //CEP não Encontrado.
        limpa_formulário_cep();
        alert("CEP não encontrado.");
    }
}
    
function pesquisacep(valor) {

    //Nova variável "cep" somente com dígitos.
    var cep = valor.replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep != "") {

        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{8}$/;

        //Valida o formato do CEP.
        if(validacep.test(cep)) {

            //Preenche os campos com "..." enquanto consulta webservice.
            document.getElementById('billing_address_1').value="...";
            document.getElementById('billing_neighborhood').value="...";
            document.getElementById('billing_city').value="...";
            document.getElementById('billing_state').value="...";
            // document.getElementById('ibge').value="...";

            //Cria um elemento javascript.
            var script = document.createElement('script');

            //Sincroniza com o callback.
            script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

            //Insere script no documento e carrega o conteúdo.
            document.body.appendChild(script);

        } //end if.
        else {
            //cep é inválido.
            limpa_formulário_cep();
            alert("Formato de CEP inválido.");
        }
    } //end if.
    else {
        //cep sem valor, limpa formulário.
        limpa_formulário_cep();
    }
};


function woo_os_ajax_fill_cliente_data(){

    var woo_os_user_email = jQuery( "#woo_os_user_email" ).val();
    // console.log(woo_os_user_email);

    var data = {
        'action': 'my_action',
        'email_do_cliente': woo_os_user_email
    };

    // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
    jQuery.post(ajaxurl, data, function(response) {
        // console.log(response);

        if(response == "false" || response == ""){
            alert("Cliente não encontrado, preencha os dados da OS para criar um novo cliente para este e-mail");
            // jQuery("#woo_os_user").val( jQuery("#woo_os_user_email").val() );
            // limpa os campos do formulario caso nao exista o cliente            
            jQuery('.complementos-pf').find('input').val('');  
            jQuery('.complementos-pj').find('input').val('');
            jQuery('.complementos-endereco').find('input').val('');

        }else{
            //recupera o json cru e parsa em um objeto para o JS

            // jQuery("#woo_os_user").val( jQuery("#woo_os_user_email").val() );
            var osCustomer = JSON.parse(response);
            var campos_vazios = 0;
            // debug
            // console.log(response);
            // preenche os campos da OS com os dados retornados do Woocommerce
            jQuery("#woo_os_user").val(osCustomer["woo_user_email"]);

            //verifica se existe o tipo de pessoa quando o usuário é criado pelo admin
            if(typeof osCustomer["billing_persontype"] === 'undefined' || osCustomer["billing_persontype"][0] === ""){
                console.log('billing_persontype não definida');
                jQuery("#billing_persontype").val('1');
                campos_vazios++;
            }else{
                jQuery("#billing_persontype").val(osCustomer["billing_persontype"][0]);

                // Preenche os campos da PF
                if(osCustomer["billing_persontype"][0] === "1"){
                    console.log('pessoa física');
                    //verifica se foi definido billing_cpf
                    if(typeof osCustomer["billing_cpf"] === 'undefined' || osCustomer["billing_cpf"][0] === ""){
                        console.log('billing_cpf não definida');
                        campos_vazios++;
                    }else{
                        jQuery("#billing_cpf").val(osCustomer["billing_cpf"][0]);
                    }

                }
                // preenche os campos da PJ
                if(osCustomer["billing_persontype"][0] === "2"){
                    console.log('pessoa juridica');
                    person_type_fields();
                    // verifica se foi definido a empresa
                    if(typeof osCustomer["billing_company"] === 'undefined' || osCustomer["billing_company"][0] === ""){
                        console.log('billing_company não definida');
                        campos_vazios++;
                    }else{
                        jQuery("#billing_company").val(osCustomer["billing_company"][0]);
                    }

                    //verifica se foi definido billing_cnpj
                    if(typeof osCustomer["billing_cnpj"] === 'undefined' || osCustomer["billing_cnpj"][0] === ""){
                        console.log('billing_cnpj não definida');
                        campos_vazios++;
                    }else{
                        jQuery("#billing_cnpj").val(osCustomer["billing_cnpj"][0]);
                    }

                    //verifica se foi definido billing_ie
                    if(typeof osCustomer["billing_ie"] === 'undefined' || osCustomer["billing_ie"][0] === ""){
                        console.log('billing_ie não definida');
                        campos_vazios++;
                    }else{
                        jQuery("#billing_ie").val(osCustomer["billing_ie"][0]);
                    }
                }
            }

            //verifica se foi definido billing_phone
            if(typeof osCustomer["billing_phone"] === 'undefined'){
                console.log('billing_phone não definida');
                campos_vazios++;
            }else{
                jQuery("#billing_phone").val(osCustomer["billing_phone"][0]);
            }

            //verifica se foi definido billing_last_name
            if(typeof osCustomer["billing_last_name"] === 'undefined'){
                console.log('billing_last_name não definida');
                campos_vazios++;
            }else{
                jQuery("#billing_last_name").val(osCustomer["billing_last_name"][0]);
            }

            //verifica se foi definido billing_first_name
            if(typeof osCustomer["billing_first_name"] === 'undefined'){
                console.log('billing_first_name não definida');
                campos_vazios++;
            }else{
                jQuery("#billing_first_name").val(osCustomer["billing_first_name"][0]);
            }

            //verifica se foi definido billing_cellphone
            if(typeof osCustomer["billing_cellphone"] === 'undefined'){
                console.log('billing_cellphone não definida');
                campos_vazios++;
            }else{
                jQuery("#billing_cellphone").val(osCustomer["billing_cellphone"][0]);
            }

            
            // teste rapido verifica se o radio do sexo está preenchido, se não estiver
            // preenche com a resposta do AJAX
            //verifica se foi definido billing_sex
            if(typeof osCustomer["billing_sex"] === 'undefined'){
                console.log('billing_sex não definida');
                campos_vazios++;
            }else{
                var jQueryradios = jQuery('input:radio[name=billing_sex]');
                if(jQueryradios.is(':checked') === false && osCustomer["billing_sex"][0] == "Masculino") {
                    jQueryradios.filter('[value="Masculino"]').prop('checked', true);
                }
                if(jQueryradios.is(':checked') === false && osCustomer["billing_sex"][0] == "Feminino") {
                    jQueryradios.filter('[value="Feminino"]').prop('checked', true);
            }
            }
            
            //verifica se foi definido billing_birthdate
            if(typeof osCustomer["billing_birthdate"] === 'undefined'){
                console.log('billing_birthdate não definida');
                campos_vazios++;
            }else{
                jQuery("#billing_birthdate").val(osCustomer["billing_birthdate"][0]);
            }

            //verifica se foi definido billing_postcode
            if(typeof osCustomer["billing_postcode"] === 'undefined'){
                console.log('billing_postcode não definida');
                campos_vazios++;
            }else{
                jQuery("#billing_postcode").val(osCustomer["billing_postcode"][0]);
                pesquisacep(osCustomer["billing_postcode"][0])
            }
            //O PREENCHIMENTO DOS CAMPOS DE ENDERECO AGORA É FEITO PELA FUNCAO pesquisacep() 

            // //verifica se foi definido billing_address_1
            // if(typeof osCustomer["billing_address_1"] === 'undefined'){
            //     console.log('billing_address_1 não definida');
            //     campos_vazios++;
            // }else{
            //     jQuery("#billing_address_1").val(osCustomer["billing_address_1"][0]);
            // }

            //verifica se foi definido billing_number
            if(typeof osCustomer["billing_number"] === 'undefined'){
                console.log('billing_number não definida');
                campos_vazios++;
            }else{
                jQuery("#billing_number").val(osCustomer["billing_number"][0]);
            }

            //verifica se foi definido billing_address_2
            if(typeof osCustomer["billing_address_2"] === 'undefined'){
                console.log('billing_address_2 não definida');
                campos_vazios++;
            }else{
                jQuery("#billing_address_2").val(osCustomer["billing_address_2"][0]);
            }

            //verifica se foi definido billing_neighborhood
            // if(typeof osCustomer["billing_neighborhood"] === 'undefined'){
            //     console.log('billing_neighborhood não definida');
            //     campos_vazios++;
            // }else{
            //     jQuery("#billing_neighborhood").val(osCustomer["billing_neighborhood"][0]);
            // }

            //verifica se foi definido billing_city
            // if(typeof osCustomer["billing_city"] === 'undefined'){
            //     console.log('billing_city não definida');
            //     campos_vazios++;
            // }else{
            //     jQuery("#billing_city").val(osCustomer["billing_city"][0]);
            // }

            //verifica se foi definido billing_state
            // if(typeof osCustomer["billing_state"] === 'undefined'){
            //     console.log('billing_state não definida');
            //     campos_vazios++;
            // }else{
            //     jQuery("#billing_state").val(osCustomer["billing_state"][0]);
            // }

            if(campos_vazios > 0){
                alert('O cliente existe mas o cadastro está incompleto, preencha os dados da os para atualizar o cliente');
            }
        }
         
    });
    
// console.log('dados do cliente preenchidos');
}
function woo_os_ajax_create_os_order(){

    var woo_os_user_email = jQuery( "#woo_os_user_email" ).val();
    var woo_os_user = jQuery( "#woo_os_user" ).val();
    var valor_da_os = jQuery( "#woo_os_valor" ).val();
    var id_da_os = jQuery( "#woo_os_number" ).val();
    var woo_os_serial = jQuery( "#woo_os_serial" ).val();
    // console.log(woo_os_user_email);

    var data = {
        'action': 'create_os_order',
        'email_do_cliente': woo_os_user_email,
        'woo_os_user': woo_os_user,
        'valor_da_os': valor_da_os,
        'id_da_os': id_da_os,
        'nr_serie_os': woo_os_serial,
    };

    // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
    jQuery.post(ajaxurl, data, function(response) {

        if(response == "false"){
            alert("OS ainda não publicada, para criar um pedido primeiro clique em Publicar");
            // console.log(response);
        }else if(response == "pedido"){
             alert("Já existe um pedido vinculado à esta OS");
        }else{
            //recupera o json cru e parsa em um objeto para o JS
            // var osCustomer = JSON.parse(response);
            // debug
            console.log(response);
            window.location.replace("/wp-admin/post.php?post="+ response +"&action=edit");
        }
    });
}
function woo_os_view_os_order(){
    var nr_do_pedido = jQuery( "#os_pedido_criado" ).val();
    window.open("/wp-admin/post.php?post="+ nr_do_pedido +"&action=edit");
    
}
function woo_os_view_os_data(){
    var nr_do_pedido = jQuery( "#os_do_pedido" ).val();
    window.open("/wp-admin/post.php?post="+ nr_do_pedido +"&action=edit");
    
}

function imprimir_ordem_de_servico(){

    var id_da_os = jQuery( "#woo_os_number" ).val();
    var woo_os_user = jQuery( "#woo_os_user" ).val();
    // console.log(woo_os_user_email);

    var data = {
        'action': 'imprimir_ordem_de_servico',
        'id_da_os': id_da_os,
        'email_do_cliente': woo_os_user,
    };

    // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
    var response = jQuery.post(ajaxurl, data, function(response) {

        if(response == "false"){
            alert("OS ainda não publicada, para imprimir uma OS primeiro clique em Publicar");
            // console.log(response);
        }else{

            console.log(response);
        }   

    });
    // return response;
    if(response){
        var thisIsAnObject = response;
        var w = window.open("/wp-content/plugins/woo-ordem-de-servico/includes/views/html-print-os.php");
        w.myVariable = thisIsAnObject;
    }
    

    
}
jQuery(function() {
    // console.log( "pronto para executar o js!" );
    mask_woo_init();
    if( !jQuery("#woo_os_user_email").val() ) {
          console.log('no client');
    }else{
        woo_os_ajax_fill_cliente_data();
    }
    jQuery('body').on("click", "#check_user", function(e){
        if( !jQuery("#woo_os_user_email").val() ) {
          alert('nenhum cliente informado');
        }else{
            woo_os_ajax_fill_cliente_data();   
        }
    });
    jQuery('body').on("click", "#criar_pedido", function(e){
         woo_os_ajax_create_os_order();      
    });
    jQuery('body').on("click", "#imprimir_os", function(e){
        imprimir_ordem_de_servico();
    });
     jQuery('body').on("click", "#ver_pedido", function(e){
        woo_os_view_os_order();
    });
     jQuery('body').on("click", "#ver_os", function(e){
        woo_os_view_os_data();
    });



});
jQuery(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = jQuery('.add_button'); //Add button selector
    var wrapper = jQuery('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div><label for="woo_os_produto[]">Tipo de Produto</label><a href="javascript:void(0);" class="remove_button"><span class="dashicons dashicons-remove"></span></a><br>';
    fieldHTML +='<input type="text" name="woo_os_produto[]" class="regular-text single-line" value=""/><br>';
    fieldHTML +='<label for="woo_os_marca[]">Marca</label><br>';
    fieldHTML +='<span type="text" class="clone" /></span><br>';
    fieldHTML +='<label for="woo_os_referencia[]">Referência</label><br>';
    fieldHTML +='<input type="text" name="woo_os_referencia[]" class="regular-text single-line" value=""/><br>';
    fieldHTML +='<label for="woo_os_problemas[]">Problema(s) declarado(s)</label><br>';
    fieldHTML +='<textarea name="woo_os_problemas[]" class="widefat" rows="4"></textarea><br></div>';
    var x = 0; //Initial field counter is 1
    
    //Once add button is clicked
    jQuery(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
           jQuery(wrapper).append(fieldHTML); //Add field html
           var inputField = jQuery('.woo_os_marca').first();
           jQuery('.clone').replaceWith(inputField.clone());
        }
    });
    
    //Once remove button is clicked
    jQuery(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        jQuery(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
// jquery search

jQuery(function($){
    // multiple select with AJAX search
    $('#woo_os_search').select2({
        ajax: {
                url: ajaxurl, // AJAX URL is predefined in WordPress admin
                dataType: 'json',
                delay: 250, // delay in ms while typing when to perform a AJAX search
                data: function (params) {
                    return {
                        q: params.term, // search query
                        action: 'search_for_os' // AJAX action for admin-ajax.php
                    };
                },
                processResults: function( data ) {
                var options = [];
                if ( data ) {
            
                    // data is the array of arrays, and each of them contains ID and the Label of the option
                    $.each( data, function( index, text ) { // do not forget that "index" is just auto incremented value
                        options.push( { id: text[0], text: text[1]  } );
                    });
                
                }
                return {
                    results: options
                };
            },
            cache: true
        },
        minimumInputLength: 3 // the minimum of symbols to input before perform a search
    });
});
jQuery(function($){
    // multiple select with AJAX search
    $('#woo_os_problem_search').select2();
});